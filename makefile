CXX = g++ -std=c++14
CXXFLAGS = -Wall -Wextra -Wshadow -Winline -Wredundant-decls -Wcast-qual -Wcast-align -Wctor-dtor-privacy -Wformat=2 -Winit-self -Wlogical-op -Wmissing-include-dirs -Wnoexcept -Woverloaded-virtual -Wstrict-null-sentinel -pedantic -pipe -march=native -O2 -pthread -m64 -I/usr/include/root
OBJ = MCanalysis.o MCanalysisDict.o
LIB = `root-config --glibs` -lRooFit -lRooFitCore -lRooStats

all: MCanalysis

clean:
	rm -rf *.exe *.o *.stackdump

new: clean all

MCanalysis: main.cpp $(OBJ)
	$(CXX) -o $@ $< $(OBJ) $(CXXFLAGS) $(LIB)

MCanalysis.o: MCanalysis.cpp MCanalysis.h
	$(CXX) -c $< $(CXXFLAGS)

# MCanalysisDict.cpp:
# 	rootcling MCanalysisDict.cpp -c -p MCanalysis.h

MCanalysisDict.o: MCanalysisDict.cpp
	$(CXX) -c $< $(CXXFLAGS)
