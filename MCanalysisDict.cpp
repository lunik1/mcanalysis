// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME MCanalysisDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "MCanalysis.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_MCanalysis(void *p = 0);
   static void *newArray_MCanalysis(Long_t size, void *p);
   static void delete_MCanalysis(void *p);
   static void deleteArray_MCanalysis(void *p);
   static void destruct_MCanalysis(void *p);
   static void streamer_MCanalysis(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MCanalysis*)
   {
      ::MCanalysis *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MCanalysis >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MCanalysis", ::MCanalysis::Class_Version(), "MCanalysis.h", 29,
                  typeid(::MCanalysis), DefineBehavior(ptr, ptr),
                  &::MCanalysis::Dictionary, isa_proxy, 16,
                  sizeof(::MCanalysis) );
      instance.SetNew(&new_MCanalysis);
      instance.SetNewArray(&newArray_MCanalysis);
      instance.SetDelete(&delete_MCanalysis);
      instance.SetDeleteArray(&deleteArray_MCanalysis);
      instance.SetDestructor(&destruct_MCanalysis);
      instance.SetStreamerFunc(&streamer_MCanalysis);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MCanalysis*)
   {
      return GenerateInitInstanceLocal((::MCanalysis*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MCanalysis*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr MCanalysis::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MCanalysis::Class_Name()
{
   return "MCanalysis";
}

//______________________________________________________________________________
const char *MCanalysis::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MCanalysis*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MCanalysis::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MCanalysis*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MCanalysis::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MCanalysis*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MCanalysis::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MCanalysis*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void MCanalysis::Streamer(TBuffer &R__b)
{
   // Stream an object of class MCanalysis.

   TSelector::Streamer(R__b);
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_MCanalysis(void *p) {
      return  p ? new(p) ::MCanalysis : new ::MCanalysis;
   }
   static void *newArray_MCanalysis(Long_t nElements, void *p) {
      return p ? new(p) ::MCanalysis[nElements] : new ::MCanalysis[nElements];
   }
   // Wrapper around operator delete
   static void delete_MCanalysis(void *p) {
      delete ((::MCanalysis*)p);
   }
   static void deleteArray_MCanalysis(void *p) {
      delete [] ((::MCanalysis*)p);
   }
   static void destruct_MCanalysis(void *p) {
      typedef ::MCanalysis current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_MCanalysis(TBuffer &buf, void *obj) {
      ((::MCanalysis*)obj)->::MCanalysis::Streamer(buf);
   }
} // end of namespace ROOT for class ::MCanalysis

namespace {
  void TriggerDictionaryInitialization_MCanalysisDict_Impl() {
    static const char* headers[] = {
"MCanalysis.h",
0
    };
    static const char* includePaths[] = {
"/usr/include/root",
"/media/sf_D_DRIVE/MEGA/Programming/Roofit/MCanalysis/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$MCanalysis.h")))  MCanalysis;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "MCanalysis.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"MCanalysis", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("MCanalysisDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_MCanalysisDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_MCanalysisDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_MCanalysisDict() {
  TriggerDictionaryInitialization_MCanalysisDict_Impl();
}
