// Global stuff

#ifndef GLOBAL_H
#define GLOBAL_H

#include <TMath.h>

namespace g {
    struct Event
    {
        enum struct category : short {primary, secondary, combinatoral};

        Event(Double_t a, Double_t b, Double_t c, Double_t d, category e)
        {
            tau = a;
            mass = b;
            ipchi2 = TMath::Log(c);
            theta = d;
            type = e;
        }

        Double_t tau;
        Double_t mass;
        Double_t ipchi2;
        Double_t theta;
        category type;
    };
}

#endif
