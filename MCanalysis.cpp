// The following methods are defined in this file:
//
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// Root > T->Process("MCanalysis.C")
// Root > T->Process("MCanalysis.C","some options")
// Root > T->Process("MCanalysis.C+")

#include "MCanalysis.h"
#include "global.h"

#include <Riostream.h>
#include <RooAddPdf.h>
#include <RooArgSet.h>
#include <RooBifurGauss.h>
#include <RooCBShape.h>
#include <RooConstVar.h>
#include <RooDataHist.h>
#include <RooDataSet.h>
#include <RooExponential.h>
#include <RooFormula.h>
#include <RooGaussian.h>
#include <RooHist.h>
#include <RooPlot.h>
#include <RooRealVar.h>
#include <RooStats/SPlot.h>
#include <TCanvas.h>
#include <TGraphAsymmErrors.h>
#include <TGraphErrors.h>
#include <TH2F.h>
#include <TLine.h>
#include <TMath.h>
#include <boost/numeric/conversion/cast.hpp>
#include <boost/range/algorithm.hpp>
#include <memory>
#include <thread>
#include <utility>
#include <vector>
// #include <TStyle.h>

using namespace RooFit;

using boost::numeric_cast;
using std::cout;
using std::endl;
using g::Event;

ClassImp(MCanalysis)

namespace {
    // Number of threads
    const Int_t THREADS{numeric_cast<Int_t>(std::thread::hardware_concurrency())};

    // create pointer to histograms
    std::unique_ptr<TH1F> hist_mass{nullptr};
    std::unique_ptr<TH1F> hist_mass_cut{nullptr};
    std::unique_ptr<TH1F> hist_ipchi2{nullptr};
    std::unique_ptr<TH1F> hist_ipchi2_cut{nullptr};
    std::unique_ptr<TH1F> hist_tau{nullptr};
    std::unique_ptr<TH2F> hist_mass_ipchi2{nullptr};

    std::vector<Event> events;

    std::vector<RooDataSet> sWeighted_datasets;

    // Define fitting range
    const Double_t MIN_M{1830};
    const Double_t MAX_M{1900};
    const Double_t MIN_IPCHI2{-6};
    const Double_t MAX_IPCHI2{9};
    RooRealVar mass{"mass", "mass", MIN_M, MAX_M};
    RooRealVar ipchi2{"ipchi2", "ipchi2", MIN_IPCHI2, MAX_IPCHI2};
    RooDataSet unbinned_data{"","", RooArgSet{mass, ipchi2}};
}  // namespace

void MCanalysis::Begin(TTree * /*tree*/)
{
    // The Begin() function is called at the start of the query.
    // When running with PROOF Begin() is only called on the client.
    // The tree argument is deprecated (on PROOF 0 is passed).

    // delete objects with same names as histograms to avoid memory issues
    delete gDirectory->FindObject("massHist");
    delete gDirectory->FindObject("massHistCut");
    delete gDirectory->FindObject("ipchi2Hist");
    delete gDirectory->FindObject("ipchi2HistCut");
    delete gDirectory->FindObject("tauHist");
    delete gDirectory->FindObject("massipchi2Hist");

    TString option{GetOption()};

    // Create histograms
    hist_mass.reset(new TH1F{"massHist", "Invariant mass", 1000, 1810, 1920});
    if (hist_mass)  // Check it got created properly
    {
        hist_mass->SetStats(false);
        hist_mass->SetLabelFont(133, "XY");
        hist_mass->SetTitleFont(133, "XY");
        hist_mass->SetLabelSize(24, "XY");
        hist_mass->SetTitleSize(24, "XY");
        hist_mass->SetTitleOffset(1.05f, "X");
        hist_mass->SetTitleOffset(1.5, "Y");
        hist_mass->SetTitle("");
        hist_mass->GetXaxis()->SetTitle("Invariant mass (MeV/c^{2})");
        hist_mass->GetYaxis()->SetTitle("Entries");
    }

    hist_mass_cut.reset(new TH1F{"massHistCut", "Invariant mass", 1000, 1810, 1920});
    if (hist_mass_cut)  // Check it got created properly
    {
        hist_mass_cut->SetStats(false);
        hist_mass_cut->SetLabelFont(133, "XY");
        hist_mass_cut->SetTitleFont(133, "XY");
        hist_mass_cut->SetLabelSize(24, "XY");
        hist_mass_cut->SetTitleSize(24, "XY");
        hist_mass_cut->SetTitleOffset(1.05f, "X");
        hist_mass_cut->SetTitleOffset(1.5, "Y");
        hist_mass_cut->SetTitle("");
        hist_mass_cut->GetXaxis()->SetTitle("Invariant mass (MeV/c^{2})");
        hist_mass_cut->GetYaxis()->SetTitle("Entries");
    }

    hist_ipchi2.reset(new TH1F{"ipchi2Hist", "Ln of impact parameter chi2",
            1000, -8, 10});
    if (hist_ipchi2)  // Check it got created properly
    {
        hist_ipchi2->SetStats(false);
        hist_ipchi2->SetLabelFont(133, "XY");
        hist_ipchi2->SetTitleFont(133, "XY");
        hist_ipchi2->SetLabelSize(24, "XY");
        hist_ipchi2->SetTitleSize(24, "XY");
        hist_ipchi2->SetTitleOffset(1.05f, "X");
        hist_ipchi2->SetTitleOffset(1.5, "Y");
        hist_ipchi2->SetTitle("");
        hist_ipchi2->GetXaxis()->SetTitle("ln(#chi^{2}_{IP})");
        hist_ipchi2->GetYaxis()->SetTitle("Entries");
    }

    hist_ipchi2_cut.reset(new TH1F{"ipchi2HistCut", "Ln of impact parameter chi2",
            1000, -8, 10});
    if (hist_ipchi2_cut)  // Check it got created properly
    {
        hist_ipchi2_cut->SetStats(false);
        hist_ipchi2_cut->SetLabelFont(133, "XY");
        hist_ipchi2_cut->SetTitleFont(133, "XY");
        hist_ipchi2_cut->SetLabelSize(24, "XY");
        hist_ipchi2_cut->SetTitleSize(24, "XY");
        hist_ipchi2_cut->SetTitleOffset(1.05f, "X");
        hist_ipchi2_cut->SetTitleOffset(1.5, "Y");
        hist_ipchi2_cut->SetTitle("");
        hist_ipchi2_cut->GetXaxis()->SetTitle("ln(#chi^{2}_{IP})");
        hist_ipchi2_cut->GetYaxis()->SetTitle("Entries");
    }

    // hist_trueid = new TH1F("trueidHist", "trueid histogram", 140, 410, 550);
    // if (hist_tau != static_cast<TH1F *> (0)) // Check it got created properly
    // {
    //     hist_trueid->GetXaxis()->SetTitle("True id");
    //     hist_trueid->GetYaxis()->SetTitle("Entries");
    // }

    hist_tau.reset(new TH1F{"tauHist", "Lifetime", 10000, -0.002, 0.002});
    if (hist_tau)  // Check it got created properly
    {
        hist_tau->SetStats(false);
        hist_tau->SetLabelFont(133, "XY");
        hist_tau->SetTitleFont(133, "XY");
        hist_tau->SetLabelSize(24, "XY");
        hist_tau->SetTitleSize(24, "XY");
        hist_tau->SetTitleOffset(1.05f, "X");
        hist_tau->SetTitleOffset(1.5, "Y");
        hist_tau->SetTitle("");
        hist_tau->GetXaxis()->SetTitle("Lifetime (ns)");
        hist_tau->GetYaxis()->SetTitle("Entries");
    }

    hist_mass_ipchi2.reset(new TH2F{"massipchi2Hist",
            "Mass and Ln of impact parameter chi2", 75, 1810, 1920, 75, -8, 10});
    if (hist_mass_ipchi2)  // Check it got created properly
    {
        hist_mass_ipchi2->SetStats(false);
        hist_mass_ipchi2->SetLabelFont(133, "XYZ");
        hist_mass_ipchi2->SetTitleFont(133, "XYZ");
        hist_mass_ipchi2->SetLabelSize(24, "XYZ");
        hist_mass_ipchi2->SetTitleSize(24, "XYZ");
        hist_mass_ipchi2->SetTitleOffset(2.3f, "X");
        hist_mass_ipchi2->SetTitleOffset(2.4f, "Y");
        hist_mass_ipchi2->SetTitleOffset(1.8f, "Z");
        hist_mass_ipchi2->SetTitle("");
        hist_mass_ipchi2->GetXaxis()->SetTitle("Mass (MeV/c^{2})");
        hist_mass_ipchi2->GetYaxis()->SetTitle("ln(#chi^{2}_{IP})");
        hist_mass_ipchi2->GetZaxis()->SetTitle("Entries");
    }

    // Reserve enough space in the vector for every event in the tree
    events.reserve(5279590);
}

void MCanalysis::SlaveBegin(TTree * /*tree*/)
{
    // The SlaveBegin() function is called after the Begin() function.
    // When running with PROOF SlaveBegin() is called on each slave server.
    // The tree argument is deprecated (on PROOF 0 is passed).

    TString option{GetOption()};
}

Bool_t MCanalysis::Process(Long64_t entry)
{
    // The Process() function is called for each entry in the tree (or possibly
    // keyed object in the case of PROOF) to be processed. The entry argument
    // specifies which entry in the currently loaded tree is to be processed.
    // It can be passed to either MCanalysis::GetEntry() or TBranch::GetEntry()
    // to read either all or the required parts of the data. When processing
    // keyed objects with PROOF, the object is already loaded and is available
    // via the fObject pointer.
    //
    // This function should contain the "body" of the analysis. It can contain
    // simple or elaborate selection criteria, run algorithms on the data
    // of the event and typically fill histograms.
    //
    // The processing can be stopped by calling Abort().
    //
    // Use fStatus to set the return value of TTree::Process().
    //
    // The return value is currently not used.

    // Get branches with entries
    b_lab1_PIDK->GetEntry(entry);
    b_lab2_PIDK->GetEntry(entry);
    // b_lab1_isMuon->GetEntry(entry);
    // b_lab2_isMuon->GetEntry(entry);
    b_lab0_M->GetEntry(entry);
    b_lab0_MINIPCHI2->GetEntry(entry);
    // b_lab0_BKGCAT->GetEntry(entry);
    b_lab0_TRUEID->GetEntry(entry);
    b_lab1_TRUEID->GetEntry(entry);
    b_lab2_TRUEID->GetEntry(entry);
    b_lab0_ID->GetEntry(entry);
    b_lab1_ID->GetEntry(entry);
    b_lab2_ID->GetEntry(entry);
    b_lab0_MC_MOTHER_ID->GetEntry(entry);
    b_lab0_MC_GD_MOTHER_ID->GetEntry(entry);
    b_lab0_MC_GD_GD_MOTHER_ID->GetEntry(entry);
    b_lab0_TAU->GetEntry(entry);
    b_lab1_TRUEP_X->GetEntry(entry);
    b_lab1_TRUEP_Y->GetEntry(entry);
    b_lab1_TRUEP_Z->GetEntry(entry);
    b_lab2_TRUEP_X->GetEntry(entry);
    b_lab2_TRUEP_Y->GetEntry(entry);
    b_lab2_TRUEP_Z->GetEntry(entry);

    // Calculate the angle between daughter particles
    Double_t lab1_TRUEP{TMath::Sqrt(
            TMath::Power(lab1_TRUEP_X, 2) +
            TMath::Power(lab1_TRUEP_Y, 2) +
            TMath::Power(lab1_TRUEP_Z, 2))};
    Double_t lab2_TRUEP{TMath::Sqrt(
            TMath::Power(lab2_TRUEP_X, 2) +
            TMath::Power(lab2_TRUEP_Y, 2) +
            TMath::Power(lab2_TRUEP_Z, 2))};
    Double_t lab1_P_DOT_lab2_P{(
            lab1_TRUEP_X * lab2_TRUEP_X +
            lab1_TRUEP_Y * lab2_TRUEP_Y +
            lab1_TRUEP_Z * lab2_TRUEP_Z)};
    Double_t THETA{180./TMath::Pi() *  // in degrees
        TMath::ACos(lab1_P_DOT_lab2_P / (lab1_TRUEP * lab2_TRUEP))};
    Double_t lab0_LNMINIPCHI2{TMath::Log(lab0_MINIPCHI2)};

    // Fill histograms

    // Uncut
    // hist_trueid->Fill(lab0_MC_MOTHER_ID);
    // hist_tau->Fill(lab0_TAU);
    hist_mass_ipchi2->Fill(lab0_M, lab0_LNMINIPCHI2);

    // Non-truth cuts
    if  (abs(lab0_ID) == 421 &&  // D0
            (lab1_PIDK > 5 || lab2_PIDK < -5) &&
            !(abs(lab1_ID) == 11 || abs(lab2_ID) == 11) &&  // not e
            !(abs(lab1_ID) == 13 || abs(lab2_ID) == 13) &&  // not muon
            !(abs(lab1_ID) == abs(lab2_ID)))  // not CP eigenstate mode
    {
        hist_tau->Fill(lab0_TAU);
        hist_ipchi2->Fill(TMath::Log(lab0_MINIPCHI2));
        hist_mass->Fill(lab0_M);

        // Restrict lifetime
        if (lab0_TAU < 0.003 && lab0_TAU > -0.002)
        {
            // Cuts to seperate primaries and secondaries
            if (abs(lab0_TRUEID) == 421 && (lab0_MC_MOTHER_ID == 0 ||
                    (abs(lab0_MC_GD_MOTHER_ID) == 0 && (
                    abs(lab0_MC_MOTHER_ID) == 411 || abs(lab0_MC_MOTHER_ID) == 413 ||
                    abs(lab0_MC_MOTHER_ID) == 415 || abs(lab0_MC_MOTHER_ID) == 421 ||
                    abs(lab0_MC_MOTHER_ID) == 423 || abs(lab0_MC_MOTHER_ID) == 425 ||
                    abs(lab0_MC_MOTHER_ID) == 431 || abs(lab0_MC_MOTHER_ID) == 433 ||
                    abs(lab0_MC_MOTHER_ID) == 435 || abs(lab0_MC_MOTHER_ID) == 445 ||
                    abs(lab0_MC_MOTHER_ID) == 10411 || abs(lab0_MC_MOTHER_ID) == 10413 ||
                    abs(lab0_MC_MOTHER_ID) == 10415 || abs(lab0_MC_MOTHER_ID) == 10421 ||
                    abs(lab0_MC_MOTHER_ID) == 10423 || abs(lab0_MC_MOTHER_ID) == 10425 ||
                    abs(lab0_MC_MOTHER_ID) == 10431 || abs(lab0_MC_MOTHER_ID) == 10433 ||
                    abs(lab0_MC_MOTHER_ID) == 10435 || abs(lab0_MC_MOTHER_ID) == 10445 ||
                    abs(lab0_MC_MOTHER_ID) == 20411 || abs(lab0_MC_MOTHER_ID) == 20413 ||
                    abs(lab0_MC_MOTHER_ID) == 20415 || abs(lab0_MC_MOTHER_ID) == 20421 ||
                    abs(lab0_MC_MOTHER_ID) == 20423 || abs(lab0_MC_MOTHER_ID) == 20425 ||
                    abs(lab0_MC_MOTHER_ID) == 20431 || abs(lab0_MC_MOTHER_ID) == 20433 ||
                    abs(lab0_MC_MOTHER_ID) == 20435 || abs(lab0_MC_MOTHER_ID) == 20445 ||
                    abs(lab0_MC_MOTHER_ID) == 30411 || abs(lab0_MC_MOTHER_ID) == 30413 ||
                    abs(lab0_MC_MOTHER_ID) == 30415 || abs(lab0_MC_MOTHER_ID) == 30421 ||
                    abs(lab0_MC_MOTHER_ID) == 30423 || abs(lab0_MC_MOTHER_ID) == 30425 ||
                    abs(lab0_MC_MOTHER_ID) == 30431 || abs(lab0_MC_MOTHER_ID) == 30433 ||
                    abs(lab0_MC_MOTHER_ID) == 30435 || abs(lab0_MC_MOTHER_ID) == 30445)) ||
                    (abs(lab0_MC_GD_GD_MOTHER_ID) == 0 && (
                    abs(lab0_MC_GD_MOTHER_ID) == 411 || abs(lab0_MC_GD_MOTHER_ID) == 413 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 415 || abs(lab0_MC_GD_MOTHER_ID) == 421 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 423 || abs(lab0_MC_GD_MOTHER_ID) == 425 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 431 || abs(lab0_MC_GD_MOTHER_ID) == 433 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 435 || abs(lab0_MC_GD_MOTHER_ID) == 445 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 10411 || abs(lab0_MC_GD_MOTHER_ID) == 10413 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 10415 || abs(lab0_MC_GD_MOTHER_ID) == 10421 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 10423 || abs(lab0_MC_GD_MOTHER_ID) == 10425 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 10431 || abs(lab0_MC_GD_MOTHER_ID) == 10433 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 10435 || abs(lab0_MC_GD_MOTHER_ID) == 10445 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 20411 || abs(lab0_MC_GD_MOTHER_ID) == 20413 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 20415 || abs(lab0_MC_GD_MOTHER_ID) == 20421 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 20423 || abs(lab0_MC_GD_MOTHER_ID) == 20425 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 20431 || abs(lab0_MC_GD_MOTHER_ID) == 20433 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 20435 || abs(lab0_MC_GD_MOTHER_ID) == 20445 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 30411 || abs(lab0_MC_GD_MOTHER_ID) == 30413 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 30415 || abs(lab0_MC_GD_MOTHER_ID) == 30421 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 30423 || abs(lab0_MC_GD_MOTHER_ID) == 30425 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 30431 || abs(lab0_MC_GD_MOTHER_ID) == 30433 ||
                    abs(lab0_MC_GD_MOTHER_ID) == 30435 || abs(lab0_MC_GD_MOTHER_ID) == 30445)) ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 411 || abs(lab0_MC_GD_GD_MOTHER_ID) == 413 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 415 || abs(lab0_MC_GD_GD_MOTHER_ID) == 421 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 423 || abs(lab0_MC_GD_GD_MOTHER_ID) == 425 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 431 || abs(lab0_MC_GD_GD_MOTHER_ID) == 433 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 435 || abs(lab0_MC_GD_GD_MOTHER_ID) == 445 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 10411 || abs(lab0_MC_GD_GD_MOTHER_ID) == 10413 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 10415 || abs(lab0_MC_GD_GD_MOTHER_ID) == 10421 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 10423 || abs(lab0_MC_GD_GD_MOTHER_ID) == 10425 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 10431 || abs(lab0_MC_GD_GD_MOTHER_ID) == 10433 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 10435 || abs(lab0_MC_GD_GD_MOTHER_ID) == 10445 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 20411 || abs(lab0_MC_GD_GD_MOTHER_ID) == 20413 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 20415 || abs(lab0_MC_GD_GD_MOTHER_ID) == 20421 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 20423 || abs(lab0_MC_GD_GD_MOTHER_ID) == 20425 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 20431 || abs(lab0_MC_GD_GD_MOTHER_ID) == 20433 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 20435 || abs(lab0_MC_GD_GD_MOTHER_ID) == 20445 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 30411 || abs(lab0_MC_GD_GD_MOTHER_ID) == 30413 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 30415 || abs(lab0_MC_GD_GD_MOTHER_ID) == 30421 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 30423 || abs(lab0_MC_GD_GD_MOTHER_ID) == 30425 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 30431 || abs(lab0_MC_GD_GD_MOTHER_ID) == 30433 ||
                    abs(lab0_MC_GD_GD_MOTHER_ID) == 30435 || abs(lab0_MC_GD_GD_MOTHER_ID) == 30445))
                events.push_back(Event(lab0_TAU, lab0_M, lab0_MINIPCHI2, THETA,
                            Event::category::primary));
            else if (abs(lab0_TRUEID) != 421)
                events.push_back(Event(lab0_TAU, lab0_M, lab0_MINIPCHI2, THETA,
                            Event::category::combinatoral));
            else
                events.push_back(Event(lab0_TAU, lab0_M, lab0_MINIPCHI2, THETA,
                            Event::category::secondary));
        }
    }


    // Truth cuts
    if (abs(lab0_TRUEID) == 421 &&  // D0
            !(abs(lab1_TRUEID) == 11 || abs(lab2_TRUEID) == 11) &&  // not e
            !(abs(lab1_TRUEID) == 13 || abs(lab2_TRUEID) == 13) &&  // not muon
            !(abs(lab1_TRUEID) == abs(lab2_TRUEID)))  // not CP eigenstate mode
    {
        hist_ipchi2_cut->Fill(TMath::Log(lab0_MINIPCHI2));
        hist_mass_cut->Fill(lab0_M);

        // Not a secondary D0
        if ((abs(lab0_MC_MOTHER_ID) == 413 || lab0_MC_MOTHER_ID == 0) &&
                lab0_MC_GD_MOTHER_ID == 0)  // not a secondary D0
        {
            // hist_ipchi2_cut->Fill(TMath::Log(lab0_MINIPCHI2));
            // hist_trueid->Fill(lab0_MC_MOTHER_ID);
        }

        // A secondary D0
        if ((abs(lab0_MC_MOTHER_ID) == 413 || lab0_MC_MOTHER_ID != 0) &&
                lab0_MC_GD_MOTHER_ID != 0)  // not a secondary D0
        {
        }
    }

    return kTRUE;
}

void MCanalysis::SlaveTerminate()
{
    // The SlaveTerminate() function is called after all entries or objects
    // have been processed. When running with PROOF SlaveTerminate() is called
    // on each slave server.
}

void MCanalysis::TauHist()
{
    // create canvas for lifetime histogram
    std::unique_ptr<TCanvas> tauCanvas{new TCanvas{"tauNoFit", "tauNoFit", 1440, 1080}};
    // draw histogram
    hist_tau->Draw();
    // save histogram
    tauCanvas->SaveAs("Lifetime.pdf");
    tauCanvas->Close();
}

Int_t MCanalysis::EqualLifetimeBins()
{
    // Shrink to fit
    events.shrink_to_fit();

    // Sort
    boost::sort(events,
            [](const Event &a, const Event &b) -> Bool_t
            {return a.tau < b.tau;});

    // Find bin edges required for bins of equal entry number
    Int_t binNo{100};  // (approximate) number of bins
    Int_t binSize{numeric_cast<Int_t>(
        ceil(numeric_cast<Double_t> (events.size()) /
         numeric_cast<Double_t> (binNo)))};  // entries wanted in each bin
    std::vector<Double_t> bins;  // bin edges
    bins.reserve(numeric_cast<size_t> (binNo));

    Int_t i{0};
    for(auto event: events)
    {
        if (i % binSize == 0) bins.push_back(event.tau);
        i++;
    }
    bins.pop_back();  // required?
    bins.push_back(0.002);  // max lifetime for final bin edge

    binNo = numeric_cast<Int_t> (bins.size());

    // Create a new histogram using the new bins
    std::unique_ptr<TH1F> flat_hist_tau{new TH1F{"flatTauHist",
            "flatLifetime", binNo, bins.data()}};
    if (flat_hist_tau) // Check it got created properly
    {
        flat_hist_tau->SetStats(false);
        flat_hist_tau->SetLabelFont(133, "XY");
        flat_hist_tau->SetTitleFont(133, "XY");
        flat_hist_tau->SetLabelSize(24, "XY");
        flat_hist_tau->SetTitleSize(24, "XY");
        flat_hist_tau->SetTitleOffset(1.05f, "X");
        flat_hist_tau->SetTitleOffset(1.5, "Y");
        flat_hist_tau->SetTitle("");
        flat_hist_tau->GetXaxis()->SetTitle("Lifetime");
        flat_hist_tau->GetYaxis()->SetTitle("Entries");
    }
    for(auto event: events)
    {
        flat_hist_tau->Fill(event.tau);
    }

    // create canvas for histogram
    std::unique_ptr<TCanvas> tauCanvas{new TCanvas{"tauNoFit", "tauNoFit", 1440, 1080}};
    // draw histogram
    flat_hist_tau->Draw();
    // save histogram
    tauCanvas->SaveAs("LifetimeFlat.pdf");
    tauCanvas->Close();

    return binSize;
}

void MCanalysis::LifetimeBinnedMassHists(const Int_t binSize)
{
    // Initialise the histogram, graphs, and canvas
    // TH1F *hist_binned_mass{new TH1F};
    std::unique_ptr<TH1F> hist_binned_mass{new TH1F{"hist_binned_mass",
            "Binned Invariant mass", 100, MIN_M, MAX_M}};
    std::unique_ptr<TCanvas> binnedMassCanvas{new TCanvas{"binnedMassNoFit", "binnedMassNoFit",
            1440, 1080}};
    TGraphErrors massGraph;
    TGraphErrors sigmaGraph;
    TGraphErrors bkgd_yieldGraph;


    // Initialise vectors to hold fit parameter values
    std::vector<Double_t> means;
    std::vector<Double_t> meanErr;
    std::vector<Double_t> meanTs;
    std::vector<Double_t> sigmas;
    std::vector<Double_t> sigmaErr;
    std::vector<Double_t> bkgd_yields;
    std::vector<Double_t> bkgd_yieldErr;

    // Varaibles required to title the plots
    Double_t minT;
    Double_t maxT;
    Double_t meanT{0};
    TString title;

    // Define fit parameters
    RooRealVar mean{"mean", "mean of Gaussian", 1840, 1890};
    RooRealVar sigma0{"sigma0", "sigma of gaussian0", 10, 4, 10};
    RooRealVar k{"k", "sigma1 = k * sigma0", 1, 0.2, 1};
    RooFormulaVar sigma1{"sigma1", "@0*@1", RooArgList(k, sigma0)};
    RooRealVar bkgd_exp_c1{"bkgd_exp_c1", "bkgd_exp_c1", -0.02, 0.};
    RooRealVar peak_yield{"peak_yield", "peak_yield0", 0, binSize / 1.};
    RooRealVar gauss0_yield{"gauss0_yield", "gauss0_yield", 0, binSize / 2.};
    RooRealVar gauss1_yield{"gauss1_yield", "gauss1_yield", 0, binSize / 2.};
    RooRealVar bkgd_yield{"bkgd_yield", "bkgd_yield", 0, binSize / 1.};
    RooArgList yields;
    yields.add(bkgd_yield);
    yields.add(peak_yield);

    // Define fit shapes
    RooGaussian gauss0{"gauss0", "gaussian0 pdf peak", mass, mean, sigma0};
    RooGaussian gauss1{"gauss1", "gaussian1 pdf peak", mass, mean, sigma1};
    RooAddPdf peak{"peak", "mass peak", RooArgList(gauss0, gauss1),
        RooArgList(gauss0_yield, gauss1_yield)};
    RooExponential bkgd_exp{"bkgd_exp", "background pdf peak", mass, bkgd_exp_c1};
    RooArgList shapes;
    shapes.add(bkgd_exp);
    shapes.add(peak);
    RooAddPdf sum{"sum", "gauss+background peak", shapes, yields};

    // define RooPlots and RooHists
    RooPlot *xframe{mass.frame()};
    RooHist *hpull;
    RooPlot *pullframe{mass.frame()};

    // Open the pdf files
    binnedMassCanvas->Print("BinnedMassHists.pdf[");
    binnedMassCanvas->Print("BinnedMassFits.pdf[");

    // Set up plots
    hist_binned_mass->SetAxisRange(0, 2600, "Y");
    hist_binned_mass->SetStats(false);
    hist_binned_mass->SetLabelFont(133, "XYt");
    hist_binned_mass->SetTitleFont(133);
    hist_binned_mass->SetTitleFont(133, "XY");
    hist_binned_mass->SetLabelSize(24, "XY");
    hist_binned_mass->SetTitleSize(24, "XY");
    hist_binned_mass->SetTitleOffset(1.05f, "X");
    hist_binned_mass->SetTitleOffset(1.5, "Y");
    hist_binned_mass->GetXaxis()->SetTitle("Invariant mass (MeV/c^{2})");
    hist_binned_mass->GetYaxis()->SetTitle("Entries");

    xframe->SetLabelFont(133, "XY");
    xframe->SetTitleFont(133, "XY");
    xframe->SetLabelSize(24, "XY");
    xframe->SetTitleSize(24, "XY");
    xframe->SetTitleOffset(1.3f, "X");
    xframe->SetTitleOffset(1.5, "Y");
    xframe->SetTitle("");
    xframe->SetXTitle("Invariant Mass (MeV/c^{2})");
    xframe->SetYTitle("Events");
    xframe->SetAxisRange(0, 2800, "Y");

    pullframe->SetLabelFont(133, "XY");
    pullframe->SetTitleFont(133, "XY");
    pullframe->SetLabelSize(24, "XY");
    pullframe->SetTitleSize(24, "XY");
    pullframe->SetTitleOffset(5, "X");
    pullframe->SetTickLength(0.02f / 0.263f, "X");
    pullframe->SetTickLength(0.02f, "Y");
    pullframe->SetTitle("");
    // pullframe->SetXTitle("ln(#chi^{2}_{IP})");
    pullframe->SetYTitle("Pull");
    pullframe->SetAxisRange(-5, 5, "Y");

    // Create a mass histogram for the mass values in every bin
    bool first{true};
    for(std::vector<Event>::reverse_iterator event = events.rbegin();
            event != events.rend(); event++)
    {
        static Int_t i {0};
        static Int_t num_events{0};

        i++;

        if (event->mass >= MIN_M && event->mass <= MAX_M)
        {
            hist_binned_mass->Fill(event->mass);
            meanT += event->tau;
            unbinned_data.add(RooArgSet{
                    RooRealVar{"mass", "mass", event->mass},
                    RooRealVar{"ipchi2", "ipchi2", event->ipchi2}});
            num_events++;
        }

        if (i % binSize == 0 || event == events.rend() - 1)
        {
            gauss0_yield.setMax(num_events);
            gauss1_yield.setMax(num_events);
            peak_yield.setMax(num_events);
            bkgd_yield.setMax(num_events);

            // Title the plot
            minT = (event - binSize < events.rbegin()) ?
                events.rbegin()->tau:
                (event - binSize)->tau;
            maxT = event->tau;
            title.Form("t = (%f,%f]", minT, maxT);
            hist_binned_mass->SetTitle(title);
            xframe->SetTitle(title);

            // Draw histogram
            hist_binned_mass->Draw("HIST E0");

            // Save histogram
            binnedMassCanvas->Print("BinnedMassHists.pdf");

            // Set gaussian fit constraints for fit paramters
            RooGaussian sigma0_con{"sigma0_con", "sigma0_con", sigma0,
                RooConst(sigma0.getVal()), RooConst(sigma0.getError())};
            RooGaussian k_con{"k_con", "k_con", k,
                RooConst(k.getVal()), RooConst(k.getError())};
            RooGaussian gauss0_yield_con{"gauss0_yield_con", "gauss0_yield_con",
                gauss0_yield, RooConst(gauss0_yield.getVal()),
                RooConst(gauss0_yield.getError())};
            RooGaussian gauss1_yield_con{"gauss1_yield_con", "gauss1_yield_con",
                gauss1_yield, RooConst(gauss1_yield.getVal()),
                RooConst(gauss1_yield.getError())};

            // Peform fit
            if (first)
                sum.fitTo(unbinned_data, Extended(kTRUE),
                        Minimizer("Minuit2", "minimize"), NumCPU(THREADS, 0));
            else
                sum.fitTo(unbinned_data, Extended(kTRUE),
                        Minimizer("Minuit2", "minimize"), NumCPU(THREADS, 0),
                        ExternalConstraints(RooArgSet(sigma0_con, k_con,
                                gauss0_yield_con, gauss1_yield_con)));

            // Perform sPlot
            // Fit paramters must be constant for sPlot
            mean.setConstant(kTRUE);
            sigma0.setConstant(kTRUE);
            k.setConstant(kTRUE);
            RooStats::SPlot sPlot{"sPlot", "sPlot", unbinned_data, &sum,
                RooArgList{peak_yield, bkgd_yield}};
            sWeighted_datasets.emplace_back(unbinned_data.GetName(),
                    unbinned_data.GetTitle(), &unbinned_data,
                    *unbinned_data.get(), nullptr, "peak_yield_sw");
            mean.setConstant(kFALSE);
            sigma0.setConstant(kFALSE);
            k.setConstant(kFALSE);

            // Plot fit
            // RooDataHist massData("massData", "Invariant mass dataset peak", mass,
            //         hist_binned_mass.get());
            unbinned_data.plotOn(xframe, Name("massHistBins"));
            sum.plotOn(xframe, Components(bkgd_exp), LineStyle(7));
            sum.plotOn(xframe, Components(peak), LineColor(kRed));
            sum.plotOn(xframe, Components(gauss0), LineStyle(2),
                    LineColor(kRed));
            sum.plotOn(xframe, Components(gauss1), LineStyle(2),
                    LineColor(kRed));
            sum.plotOn(xframe, Name("sum"));
            unbinned_data.plotOn(xframe, Name("massHistBins"));
            binnedMassCanvas->Clear();

            // Create pull plot
            hpull = xframe->pullHist();

            // Create pull plot
            pullframe->addPlotable(hpull, "EXB");
            pullframe->SetAxisRange(-5, 5, "Y");

            // Plot fits
            binnedMassCanvas->Divide(2);
            binnedMassCanvas->cd(1);
            binnedMassCanvas->cd(1);
            gPad->SetPad(0.005, .24, 1., 1.);
            xframe->Draw();
            binnedMassCanvas->cd(2);
            gPad->SetPad(0.005 , 0.02, 1., .265);
            pullframe->Draw();
            binnedMassCanvas->Print("BinnedMassFits.pdf");

            // Fill fit paramater vectors
            meanTs.emplace_back(meanT / num_events);
            meanT = 0;
            means.emplace_back(mean.getVal());
            meanErr.emplace_back(mean.getError());
            sigmas.emplace_back(hist_binned_mass->GetRMS());
            sigmaErr.emplace_back(hist_binned_mass->GetRMSError());
            bkgd_yields.emplace_back(bkgd_yield.getVal() /
                    (bkgd_yield.getVal() + peak_yield.getVal()));
            bkgd_yieldErr.emplace_back(bkgd_yield.getError() /
                    (bkgd_yield.getVal() + peak_yield.getVal()));

            // Empty histogram
            hist_binned_mass->Reset();
            unbinned_data.reset();

            // Clear RooPlots and Canvas
            TObject *obj{xframe->findObject(nullptr)};  // most recent object
            while (obj)  // while there are still objects
            {
                xframe->remove();  // delete objects from xframe
                obj = xframe->findObject(nullptr);
            }
            obj = pullframe->findObject(nullptr);
            while (obj)
            {
                pullframe->remove();
                obj = pullframe->findObject(nullptr);
            }
            binnedMassCanvas->Clear();

            i = 0;
            num_events = 0;
            first = false;
        }
    }

    // Plot graphs of paramaters
    // gStyle->SetEndErrorSize(5);

    // Mean mass
    massGraph = TGraphErrors(numeric_cast<Int_t> (meanTs.size()),
            meanTs.data(), means.data(), nullptr, meanErr.data());
    massGraph.GetXaxis()->SetLabelFont(133);
    massGraph.GetYaxis()->SetLabelFont(133);
    massGraph.GetXaxis()->SetTitleFont(133);
    massGraph.GetYaxis()->SetTitleFont(133);
    massGraph.GetXaxis()->SetLabelSize(24);
    massGraph.GetYaxis()->SetLabelSize(24);
    massGraph.GetXaxis()->SetTitleSize(24);
    massGraph.GetYaxis()->SetTitleSize(24);
    massGraph.GetXaxis()->SetTitleOffset(1.3f);
    massGraph.GetYaxis()->SetTitleOffset(1.5);
    massGraph.SetTitle("");
    // massGraph.SetTitle("Mean Mass inside Lifetime Bins");
    massGraph.GetXaxis()->SetTitle("Lifetime bin centre (ns)");
    massGraph.GetYaxis()->SetTitle("Mean Mass (MeV/c^{2})");
    massGraph.GetXaxis()->SetLimits(-0.002, 0.002);
    massGraph.SetMarkerSize(1);
    massGraph.SetMarkerStyle(5);
    massGraph.Draw("AP");
    binnedMassCanvas->Print("MeanMassvLifetime.pdf");
    binnedMassCanvas->Clear();

    // Sigma of histogram
    sigmaGraph = TGraphErrors(numeric_cast<Int_t>(meanTs.size()),
            meanTs.data(), sigmas.data(), nullptr, sigmaErr.data());
    sigmaGraph.GetXaxis()->SetLabelFont(133);
    sigmaGraph.GetYaxis()->SetLabelFont(133);
    sigmaGraph.GetXaxis()->SetTitleFont(133);
    sigmaGraph.GetYaxis()->SetTitleFont(133);
    sigmaGraph.GetXaxis()->SetLabelSize(24);
    sigmaGraph.GetYaxis()->SetLabelSize(24);
    sigmaGraph.GetXaxis()->SetTitleSize(24);
    sigmaGraph.GetYaxis()->SetTitleSize(24);
    sigmaGraph.GetXaxis()->SetTitleOffset(1.3f);
    sigmaGraph.GetYaxis()->SetTitleOffset(1.5);
    sigmaGraph.SetTitle("");
    // sigmaGraph.SetTitle("Standard Deviation inside Lifetime Bins");
    sigmaGraph.GetXaxis()->SetTitle("Lifetime bin centre (ns)");
    sigmaGraph.GetYaxis()->SetTitle("Standard deviation");
    sigmaGraph.GetXaxis()->SetLimits(-0.002, 0.002);
    sigmaGraph.SetMarkerSize(1);
    sigmaGraph.SetMarkerStyle(5);
    sigmaGraph.Draw("AP");
    binnedMassCanvas->Print("RMSvLifetime.pdf");
    binnedMassCanvas->Clear();

    // Background yield
    bkgd_yieldGraph = TGraphErrors(numeric_cast<Int_t>(meanTs.size()),
            meanTs.data(), bkgd_yields.data(), nullptr, bkgd_yieldErr.data());
    bkgd_yieldGraph.GetXaxis()->SetLabelFont(133);
    bkgd_yieldGraph.GetYaxis()->SetLabelFont(133);
    bkgd_yieldGraph.GetXaxis()->SetTitleFont(133);
    bkgd_yieldGraph.GetYaxis()->SetTitleFont(133);
    bkgd_yieldGraph.GetXaxis()->SetLabelSize(24);
    bkgd_yieldGraph.GetYaxis()->SetLabelSize(24);
    bkgd_yieldGraph.GetXaxis()->SetTitleSize(24);
    bkgd_yieldGraph.GetYaxis()->SetTitleSize(24);
    bkgd_yieldGraph.GetXaxis()->SetTitleOffset(1.3f);
    bkgd_yieldGraph.GetYaxis()->SetTitleOffset(1.5);
    bkgd_yieldGraph.SetTitle("");
    // bkgd_yieldGraph.SetTitle("Background Yield inside Lifetime Bins");
    bkgd_yieldGraph.GetXaxis()->SetTitle("Lifetime bin centre (ns)");
    bkgd_yieldGraph.GetYaxis()->SetTitle("Background yield");
    bkgd_yieldGraph.GetXaxis()->SetLimits(-0.002, 0.002);
    bkgd_yieldGraph.GetYaxis()->SetRangeUser(0, 1);
    bkgd_yieldGraph.SetMarkerSize(1);
    bkgd_yieldGraph.SetMarkerStyle(5);
    bkgd_yieldGraph.Draw("AP");
    binnedMassCanvas->Print("MassBackgroundvLifetime.pdf");
    binnedMassCanvas->Clear();

    // Close the pdf files
    binnedMassCanvas->Print("BinnedMassHists.pdf]");
    binnedMassCanvas->Print("BinnedMassFits.pdf]");
    binnedMassCanvas->Close();
}

void MCanalysis::LifetimeBinnedIPChi2Hists(const Int_t binSize)
{
    // Initialise the histogram, graphs, and canvas
    std::unique_ptr<TH1F> hist_binned_ipchi2{new TH1F{"hist_binned_ipchi2",
            "Binned Ln IP Chi2", 100, MIN_IPCHI2, MAX_IPCHI2}};
    std::unique_ptr<TCanvas> binnedIPChi2Canvas{new TCanvas{"binnedIPChi2NoFit",
            "binnedIChi2NoFit", 1440, 1080}};
    TGraphErrors meanGraph;
    // TGraphErrors secondaryGraph;

    // Initialise vectors to hold fit parameter values
    std::vector<Double_t> meanTs;
    std::vector<Double_t> means;
    std::vector<Double_t> meanErr;
    // std::vector<Double_t> secondary;
    // std::vector<Double_t> secondaryErr;

    // Varaibles required to title the plots
    Double_t minT;
    Double_t maxT;
    Double_t meanT{0};
    TString title;

    // Define fit parameters
    RooRealVar mean0{"mean0", "mean of Gaussians", -0.5, 1.5};
    RooRealVar mean1{"mean1", "mean of lone Gaussian", 5.5, 2, 5.5};
    RooRealVar sigmal_0{"sigmal_0", "left  sigma of Gaussian0", 1, 0.7, 3};
    RooRealVar sigmar_0{"sigmar_0", "right sigma of Gaussian0", 1, 0.7, 2};
    // RooRealVar kl{"kl", "kl", 1, 1, 4};
    // RooRealVar kr{"kr", "kr", 1, 1, 4};
    // RooFormulaVar sigmal_1{"sigma1", "@0*@1", RooArgList(kl, sigmal_0)};
    // RooFormulaVar sigmar_1{"sigma1", "@0*@1", RooArgList(kr, sigmar_0)};
    RooRealVar sigmal_1{"sigmal_1", "left  sigma of Gaussian1", 1, 0.7, 3};
    RooRealVar sigmar_1{"sigmar_1", "right sigma of Gaussian1", 1, 0.7, 2};
    RooRealVar sigmal_2{"sigmal_3", "left  sigma of Gaussian2", 3, 1.7, 2.5};
    RooRealVar sigmar_2{"sigmar_2", "right sigma of Gaussian2", 2, 0.7, 3};
    RooRealVar gauss_yield0{"gauss_yield0", "gauss_yield0", 0, binSize / 1.};
    RooRealVar gauss_yield1{"gauss_yield1", "gauss_yield1", 0, binSize / 1.};
    RooRealVar gauss_yield2{"gauss_yield3", "gauss_yield2", 0, binSize / 1.};
    RooRealVar primary_yield{"primary_yield", "primary_yield",
         0, 0, binSize / 1.};
    RooArgList yields;
    yields.add(primary_yield);
    yields.add(gauss_yield2);

    // define fit shapes
    RooBifurGauss gauss0{"gauss0", "gaussian0", ipchi2, mean0, sigmal_0, sigmar_0};
    RooBifurGauss gauss1{"gauss1", "gaussian1", ipchi2, mean0, sigmal_1, sigmar_1};
    RooBifurGauss gauss2{"gauss2", "gaussian2", ipchi2, mean1, sigmal_2, sigmar_2};
    RooAddPdf primary{"primary", "primary peak",
        RooArgList(gauss0, gauss1), RooArgList(gauss_yield0, gauss_yield1)};
    RooArgList shapes;
    shapes.add(primary);
    shapes.add(gauss2);
    RooAddPdf sum{"sum", "Total ln ipchi2 pdf", shapes, yields};

    // define RooPlots and RooHists
    RooPlot *xframe{ipchi2.frame()};
    RooPlot *sPlot_frame{ipchi2.frame()};
    RooHist *hpull;
    RooPlot *pullframe{ipchi2.frame()};

    // Open the pdf files
    binnedIPChi2Canvas->Print("BinnedIPChi2Hists.pdf[");
    binnedIPChi2Canvas->Print("BinnedIPChi2Fits.pdf[");
    binnedIPChi2Canvas->Print("SPlot.pdf[");

    // Set up plots
    hist_binned_ipchi2->SetStats(false);
    hist_binned_ipchi2->SetLabelFont(133, "XYt");
    hist_binned_ipchi2->SetTitleFont(133);
    hist_binned_ipchi2->SetTitleFont(133, "XY");
    hist_binned_ipchi2->SetLabelSize(24, "XY");
    hist_binned_ipchi2->SetTitleSize(24, "XY");
    hist_binned_ipchi2->SetTitleOffset(1.05f, "X");
    hist_binned_ipchi2->SetTitleOffset(1.5, "Y");
    hist_binned_ipchi2->GetXaxis()->SetTitle("ln(#chi^{2}_{IP})");
    hist_binned_ipchi2->GetYaxis()->SetTitle("Entries");
    hist_binned_ipchi2->SetAxisRange(0, 2000, "Y");

    xframe->SetLabelFont(133, "XY");
    xframe->SetTitleFont(133, "XY");
    xframe->SetLabelSize(24, "XY");
    xframe->SetTitleSize(24, "XY");
    xframe->SetTitleOffset(1.3f, "X");
    xframe->SetTitleOffset(1.5, "Y");
    xframe->SetTitle("");
    xframe->SetXTitle("ln(#chi^{2}_{IP})");
    xframe->SetYTitle("Events");
    xframe->SetAxisRange(0, 2000, "Y");

    sPlot_frame->SetLabelFont(133, "XY");
    sPlot_frame->SetTitleFont(133, "XY");
    sPlot_frame->SetLabelSize(24, "XY");
    sPlot_frame->SetTitleSize(24, "XY");
    sPlot_frame->SetTitleOffset(1.3f, "X");
    sPlot_frame->SetTitleOffset(1.5, "Y");
    sPlot_frame->SetTitle("");
    sPlot_frame->SetXTitle("ln(#chi^{2}_{IP})");
    sPlot_frame->SetYTitle("Events");
    sPlot_frame->SetAxisRange(0, 2000, "Y");

    pullframe->SetAxisRange(-4, 4, "Y");
    pullframe->SetLabelFont(133, "XY");
    pullframe->SetTitleFont(133, "XY");
    pullframe->SetLabelSize(24, "XY");
    pullframe->SetTitleSize(24, "XY");
    pullframe->SetTitleOffset(5, "X");
    pullframe->SetTickLength(numeric_cast<Float_t> (0.02 / 0.263), "X");
    pullframe->SetTickLength(0.02f, "Y");
    pullframe->SetTitle("");
    // pullframe->SetXTitle("ln(#chi^{2}_{IP})");
    pullframe->SetYTitle("Pull");

    // Create a IPChi2 histogram for the IPChi2 values in every bin
    for(std::vector<Event>::reverse_iterator event = events.rbegin();
            event != events.rend(); event++)
    {
        static bool first{true};
        static Int_t i{0};
        static Int_t events_this_bin{0};
        static Int_t events_last_bin{0};

        if (event->type != Event::category::combinatoral)
            hist_binned_ipchi2->Fill(event->ipchi2);

        meanT += event->tau;

        i++;

        if (i % binSize == 0 || event == events.rend() - 1)
        {
            static size_t j = 0;

            events_this_bin = sWeighted_datasets.at(j).numEntries();

            primary_yield.setMax(events_this_bin);
            gauss_yield0.setMax(events_this_bin);
            gauss_yield1.setMax(events_this_bin);
            gauss_yield2.setMax(events_this_bin);

            // Title the plot
            minT = (event - binSize < events.rbegin()) ?
                events.rbegin()->tau:
                (event - binSize)->tau;
            maxT = event->tau;
            title.Form("t = (%f,%f]", minT, maxT);
            hist_binned_ipchi2->SetTitle(title);
            xframe->SetTitle(title);

            // Draw histogram
            hist_binned_ipchi2->Draw();
            // Save histogram
            binnedIPChi2Canvas->Print("BinnedIPChi2Hists.pdf");

            // Compare true and sPlot unfolded lnipchi2 distributions
            RooDataHist ipchi2data{"ipchi2data", "ln ipchi2 dataset peak", ipchi2,
                    hist_binned_ipchi2.get()};
            binnedIPChi2Canvas->Clear();
            ipchi2data.plotOn(sPlot_frame, Name("ipchi2HistBins"));
            sWeighted_datasets.at(j).plotOn(sPlot_frame, DataError(RooAbsData::SumW2),
                    MarkerColor(kRed), LineColor(kRed));
            sPlot_frame->Draw();
            binnedIPChi2Canvas->Print("SPlot.pdf");

            // Set fit constraints
            RooGaussian sigmal_0_con{"sigmal_0_con", "sigmal_0_con", sigmal_0,
                RooConst(sigmal_0.getVal()), RooConst(sigmal_0.getError())};
            RooGaussian sigmar_0_con{"sigmar_0_con", "sigmar_0_con", sigmar_0,
                RooConst(sigmar_0.getVal()), RooConst(sigmar_0.getError())};
            RooGaussian sigmal_1_con{"sigmal_1_con", "sigmal_1_con", sigmal_1,
                RooConst(sigmal_1.getVal()), RooConst(sigmal_1.getError())};
            RooGaussian sigmar_1_con{"sigmar_1_con", "sigmar_1_con", sigmar_1,
                RooConst(sigmar_1.getVal()), RooConst(sigmar_1.getError())};
            // RooGaussian kl_con{"kl_con", "kl_con", kl,
            //     RooConst(kl.getVal()), RooConst(3 * kl.getError())};
            // RooGaussian kr_con{"kr_con", "kr_con", kr,
            //     RooConst(kr.getVal()), RooConst(3 * kr.getError())};
            RooGaussian sigmal_2_con{"sigmal_2_con", "sigmal_2_con", sigmal_2,
                RooConst(sigmal_2.getVal()), RooConst(sigmal_2.getError())};
            RooGaussian sigmar_2_con{"sigmar_2_con", "sigmar_2_con", sigmar_2,
                RooConst(sigmar_2.getVal()), RooConst(sigmar_2.getError())};
            RooGaussian gauss_yield0_con{"gauss_yield0_con", "gauss_yield0_con", gauss_yield0,
                RooConst(gauss_yield0.getVal()), RooConst(gauss_yield0.getError())};
            RooGaussian gauss_yield1_con{"gauss_yield1_con", "gauss_yield1_con", gauss_yield1,
                RooConst(gauss_yield1.getVal()), RooConst(gauss_yield1.getError())};
            RooBifurGauss gauss_yield2_con{"gauss_yield2_con", "gauss_yield2_con", gauss_yield2,
                RooConst(gauss_yield2.getVal()), RooConst(3 * gauss_yield2.getError()),
                RooConst(0.5 * gauss_yield2.getError())};
            RooBifurGauss mean1_con{"mean1_con", "mean1_con", mean1,
                RooConst(mean1.getVal()), RooConst(3 * mean1.getError()),
                RooConst(0.5 * mean1.getError())};
            RooArgSet constraints{sigmal_0_con, sigmar_0_con,
                        sigmal_1_con, sigmar_1_con, sigmal_2_con,
                        // kl_con, kr_con, sigmal_2_con,
                        sigmar_2_con, gauss_yield0_con,
                        gauss_yield1_con, gauss_yield2_con};
            constraints.add(mean1_con);

            // Peform fit
            // binned
           RooDataHist sPlot_ipchi2_binned{"sPlot_ipchi2_binned",
                "sPlot_ipchi2_binned", ipchi2, sWeighted_datasets.at(j)};
            if (gauss_yield2.getVal() / primary_yield.getVal() >= .3)
            {
                sum.fitTo(sPlot_ipchi2_binned, SumW2Error(kTRUE), Binned(kTRUE),
                        Extended(kTRUE), Minimizer("Minuit2", "minimize"),
                        NumCPU(THREADS, 0), PrintLevel(-1));
                sum.fitTo(sPlot_ipchi2_binned, SumW2Error(kTRUE), Binned(kTRUE),
                        Extended(kTRUE), Minimizer("Minuit2", "minimize"),
                        NumCPU(THREADS, 0), PrintLevel(-1));
            }
            else
            {
                sum.fitTo(sPlot_ipchi2_binned, SumW2Error(kTRUE), Binned(kTRUE),
                        Extended(kTRUE), Minimizer("Minuit2", "scan"),
                        ExternalConstraints(constraints),
                        NumCPU(THREADS, 0), PrintLevel(-1));
            }
            // unbinned
            // if (gauss_yield2.getVal() / primary_yield.getVal() >= .3)
            // {
            //     sum.fitTo(sWeighted_datasets.at(j), SumW2Error(kTRUE), Binned(kTRUE),
            //             Extended(kTRUE), Minimizer("Minuit2", "minimize"),
            //             NumCPU(THREADS, 0), PrintLevel(-1));
            //     sum.fitTo(sWeighted_datasets.at(j), SumW2Error(kTRUE), Binned(kTRUE),
            //             Extended(kTRUE), Minimizer("Minuit2", "minimize"),
            //             NumCPU(THREADS, 0), PrintLevel(-1));
            // }
            // else
            // {
            //     sum.fitTo(sWeighted_datasets.at(j), SumW2Error(kTRUE), Binned(kTRUE),
            //             Extended(kTRUE), Minimizer("Minuit2", "scan"),
            //             ExternalConstraints(constraints),
            //             NumCPU(THREADS, 0), PrintLevel(-1));
            // }

            // Plot fit
            // ipchi2data.plotOn(xframe, Name("ipchi2HistBins"));
            sWeighted_datasets.at(j).plotOn(xframe, DataError(RooAbsData::SumW2));
            sum.plotOn(xframe, Components(primary), LineStyle(2),
                    LineColor(kBlue));
            sum.plotOn(xframe, Components(gauss0), LineStyle(2),
                    LineColor(kGreen+1));
            sum.plotOn(xframe, Components(gauss1), LineStyle(2),
                    LineColor(kGreen+1));
            sum.plotOn(xframe, Components(gauss2), LineStyle(2),
                    LineColor(kMagenta+1));
            sum.plotOn(xframe, Name("sum"));
            sWeighted_datasets.at(j).plotOn(xframe, DataError(RooAbsData::SumW2));
            xframe->SetAxisRange(0, 2000, "Y");
            binnedIPChi2Canvas->Clear();

            // Create pull plot
            hpull = xframe->pullHist();

            // Create pull plot
            pullframe->addPlotable(hpull, "EXB");
            pullframe->SetAxisRange(-5, 5, "Y");

            // Plot fits
            binnedIPChi2Canvas->Divide(2);
            binnedIPChi2Canvas->cd(1);
            gPad->SetPad(0.005, .24, 1., 1.);
            xframe->Draw();
            binnedIPChi2Canvas->cd(2);
            gPad->SetPad(0.005 , 0.02, 1., .265);
            pullframe->Draw();
            binnedIPChi2Canvas->Print("BinnedIPChi2Fits.pdf");

            // Fill paramater vectors
            meanTs.push_back(meanT / events_this_bin);
            meanT = 0;
            means.push_back(mean0.getVal());
            meanErr.push_back(mean0.getError());
            // secondary.push_back(peak_yield3.getVal());
            // secondaryErr.push_back(peak_yield3.getVal());

            // Empty histogram
            hist_binned_ipchi2->Reset();

            // Clear RooPlots and Canvas
            TObject *obj{xframe->findObject(nullptr)};  // most recent object
            while (obj)  // while there are still objects
            {
                xframe->remove();  // delete objects from xframe
                obj = xframe->findObject(nullptr);
            }
            obj = pullframe->findObject(nullptr);
            while (obj)
            {
                pullframe->remove();
                obj = pullframe->findObject(nullptr);
            }
            obj = sPlot_frame->findObject(nullptr);
            while (obj)
            {
                sPlot_frame->remove();
                obj = sPlot_frame->findObject(nullptr);
            }
            binnedIPChi2Canvas->Clear();

            i = 0;
            events_last_bin = events_this_bin;
            events_this_bin = 0;
            first = false;

            j++;
        }
    }

    // Plot graphs of paramaters
    // gStyle->SetEndErrorSize(5);

    // Mean of main peak
    meanGraph = TGraphErrors(numeric_cast<Int_t> (meanTs.size()),
            meanTs.data(), means.data(), nullptr, meanErr.data());
    meanGraph.GetXaxis()->SetLabelFont(133);
    meanGraph.GetYaxis()->SetLabelFont(133);
    meanGraph.GetXaxis()->SetTitleFont(133);
    meanGraph.GetYaxis()->SetTitleFont(133);
    meanGraph.GetXaxis()->SetLabelSize(24);
    meanGraph.GetYaxis()->SetLabelSize(24);
    meanGraph.GetXaxis()->SetTitleSize(24);
    meanGraph.GetYaxis()->SetTitleSize(24);
    meanGraph.GetXaxis()->SetTitleOffset(1.3f);
    meanGraph.GetYaxis()->SetTitleOffset(1.5);
    meanGraph.SetTitle("");
    // meanGraph.SetTitle("Mean of Primary IPChi2 peak inside Lifetime Bins");
    meanGraph.GetXaxis()->SetTitle("Lifetime bin centre (ns)");
    meanGraph.GetYaxis()->SetTitle("Mean ln(#chi^{2}_{IP})");
    meanGraph.GetXaxis()->SetLimits(-0.002, 0.002);
    meanGraph.SetMarkerSize(1);
    meanGraph.SetMarkerStyle(5);
    meanGraph.Draw("AP");
    binnedIPChi2Canvas->Print("MeanIPChi2vLifetime.pdf");
    binnedIPChi2Canvas->Clear();

    // Yield of secondary peak
    // secondaryGraph = TGraphErrors(numeric_cast<Int_t> (meanTs.size()),
    //         meanTs.data(), secondary.data(), 0, secondaryErr.data());
    // secondaryGraph.SetTitle("Mean Mass inside Lifetime Bins");
    // secondaryGraph.GetXaxis()->SetTitle("Lifetime bin centre");
    // secondaryGraph.GetYaxis()->SetTitle("Secondary peak yield");
    // secondaryGraph.SetMarkerSize(1);
    // secondaryGraph.SetMarkerStyle(5);
    // secondaryGraph.Draw("AP");
    // binnedIPChi2Canvas->Print("SecondaryIPChi2vLifetime.pdf");
    // binnedIPChi2Canvas->Clear();

    // Close the pdf files
    binnedIPChi2Canvas->Print("BinnedIPChi2Hists.pdf]");
    binnedIPChi2Canvas->Print("BinnedIPChi2Fits.pdf]");
    binnedIPChi2Canvas->Print("SPlot.pdf]");
    binnedIPChi2Canvas->Close();
}

void MCanalysis::LifetimeBinnedThetaHists(const Int_t binSize)
{
    // Initialise the histogram, graphs, and canvas
    std::unique_ptr<TH1F> hist_binned_theta{new TH1F{"hist_binned_theta",
            "Binned Theta", 100, 0, 60}};
    std::unique_ptr<TCanvas> binnedThetaCanvas{new TCanvas{"binnedThetaNoFit",
            "binnedThetaNoFit", 1440, 1080}};
    TGraphErrors meanGraph;
    TGraphErrors rmsGraph;

    // Initialise vector to hold means
    std::vector<Double_t> meanTs;
    std::vector<Double_t> means;
    std::vector<Double_t> meanErr;
    std::vector<Double_t> rms;
    std::vector<Double_t> rmsErr;

    // Varaibles required to title the plots
    Double_t minT;
    Double_t maxT;
    Double_t meanT{0};
    TString title;

    // Open the pdf files
    binnedThetaCanvas->Print("BinnedThetaHists.pdf[");

    // Setup plots
    hist_binned_theta->SetStats(false);
    hist_binned_theta->SetLabelFont(133, "XYt");
    hist_binned_theta->SetTitleFont(133);
    hist_binned_theta->SetTitleFont(133, "XY");
    hist_binned_theta->SetLabelSize(24, "XY");
    hist_binned_theta->SetTitleSize(24, "XY");
    hist_binned_theta->SetTitleOffset(1.05f, "X");
    hist_binned_theta->SetTitleOffset(1.5, "Y");
    hist_binned_theta->GetXaxis()->SetTitle("#theta");
    hist_binned_theta->GetYaxis()->SetTitle("Entries");
    hist_binned_theta->SetAxisRange(0, 3000, "Y");

    // Create a Theta histogram for the Theta values in every bin
    Int_t i{0};
    for(std::vector<Event>::reverse_iterator event = events.rbegin();
            event != events.rend(); event++)
    {
        hist_binned_theta->Fill(event->theta);
        meanT += event->tau;
        i++;

        if (i % binSize == 0 || event == events.rend() - 1)
        {
            // Title the plot
            minT = (event - binSize < events.rbegin())?
                events.rbegin()->tau:
                (event - binSize)->tau;
            maxT = event->tau;
            title.Form("t = (%f,%f]", minT, maxT);
            hist_binned_theta->SetTitle(title);

            // Populate vectors
            meanTs.push_back(meanT / binSize);
            meanT = 0;
            means.push_back(hist_binned_theta->GetMean());
            meanErr.push_back(hist_binned_theta->GetMeanError());
            rms.push_back(hist_binned_theta->GetRMS());
            rmsErr.push_back(hist_binned_theta->GetRMSError());

            // Draw histogram
            hist_binned_theta->Draw();
            // Save histogram
            binnedThetaCanvas->Print("BinnedThetaHists.pdf");

            // Empty histogram
            hist_binned_theta->Reset();

            // Clear Canvas
            binnedThetaCanvas->Clear();
        }
    }

    binnedThetaCanvas->Print("BinnedThetaHists.pdf]");

    meanGraph = TGraphErrors(numeric_cast<Int_t> (meanTs.size()),
            meanTs.data(), means.data(), nullptr, meanErr.data());
    meanGraph.GetXaxis()->SetLabelFont(133);
    meanGraph.GetYaxis()->SetLabelFont(133);
    meanGraph.GetXaxis()->SetTitleFont(133);
    meanGraph.GetYaxis()->SetTitleFont(133);
    meanGraph.GetXaxis()->SetLabelSize(24);
    meanGraph.GetYaxis()->SetLabelSize(24);
    meanGraph.GetXaxis()->SetTitleSize(24);
    meanGraph.GetYaxis()->SetTitleSize(24);
    meanGraph.GetXaxis()->SetTitleOffset(1.3f);
    meanGraph.GetYaxis()->SetTitleOffset(1.5);
    meanGraph.SetTitle("");
    // meanGraph.SetTitle("Mean of Primary IPChi2 peak inside Lifetime Bins");
    meanGraph.GetXaxis()->SetTitle("Lifetime bin centre (ns)");
    meanGraph.GetYaxis()->SetTitle("Mean #theta");
    meanGraph.GetXaxis()->SetLimits(-0.002, 0.002);
    meanGraph.SetMarkerSize(1);
    meanGraph.SetMarkerStyle(5);
    meanGraph.Draw("AP");
    binnedThetaCanvas->Print("MeanThetavLifetime.pdf");
    binnedThetaCanvas->Clear();

    rmsGraph = TGraphErrors(numeric_cast<Int_t> (meanTs.size()),
            meanTs.data(), rms.data(), 0, rmsErr.data());
    rmsGraph.GetXaxis()->SetLabelFont(133);
    rmsGraph.GetYaxis()->SetLabelFont(133);
    rmsGraph.GetXaxis()->SetTitleFont(133);
    rmsGraph.GetYaxis()->SetTitleFont(133);
    rmsGraph.GetXaxis()->SetLabelSize(24);
    rmsGraph.GetYaxis()->SetLabelSize(24);
    rmsGraph.GetXaxis()->SetTitleSize(24);
    rmsGraph.GetYaxis()->SetTitleSize(24);
    rmsGraph.GetXaxis()->SetTitleOffset(1.3f);
    rmsGraph.GetYaxis()->SetTitleOffset(1.5);
    rmsGraph.SetTitle("");
    // rmsGraph.SetTitle("Mean of Primary IPChi2 peak inside Lifetime Bins");
    rmsGraph.GetXaxis()->SetTitle("Lifetime bin mean (ns)");
    rmsGraph.GetYaxis()->SetTitle("RMS of #theta");
    rmsGraph.GetXaxis()->SetLimits(-0.002, 0.002);
    rmsGraph.SetMarkerSize(1);
    rmsGraph.SetMarkerStyle(5);
    rmsGraph.Draw("AP");
    binnedThetaCanvas->Print("ThetaRMSvLifetime.pdf");
    binnedThetaCanvas->Clear();

    binnedThetaCanvas->Close();
}

void MCanalysis::LifetimeBinnedPrimaryHists(const Int_t binSize)
{
    // Initialise the histogram, graphs, and canvas
    std::unique_ptr<TH1F> hist_binned_primary{new TH1F{"hist_binned_primary",
            "Binned Primary", 100, MIN_IPCHI2, MAX_IPCHI2}};
    std::unique_ptr<TCanvas> binnedPrimaryCanvas{new TCanvas{"binnedPrimaryNoFit",
            "binnedPrimaryNoFit", 1440, 1080}};

    // Initialise vector to hold means
    std::vector<Double_t> meanTs;

    // Varaibles required to title the plots
    Double_t minT;
    Double_t maxT;
    Double_t meanT{0};
    TString title;

    // Open the pdf files
    binnedPrimaryCanvas->Print("BinnedPrimaryHists.pdf[");

    // Setup plots
    hist_binned_primary->SetStats(kTRUE);
    hist_binned_primary->SetLabelFont(133, "XYt");
    hist_binned_primary->SetTitleFont(133);
    hist_binned_primary->SetTitleFont(133, "XY");
    hist_binned_primary->SetLabelSize(24, "XY");
    hist_binned_primary->SetTitleSize(24, "XY");
    hist_binned_primary->SetTitleOffset(1.05f, "X");
    hist_binned_primary->SetTitleOffset(1.5, "Y");
    hist_binned_primary->GetXaxis()->SetTitle("ln(#chi^{2}_{IP})");
    hist_binned_primary->GetYaxis()->SetTitle("Entries");
    hist_binned_primary->SetAxisRange(0, 2000, "Y");

    // Create a Primary histogram for the Primary values in every bin
    Int_t i{0};
    for(std::vector<Event>::reverse_iterator event = events.rbegin();
            event != events.rend(); event++)
    {
        if (event->type == Event::category::primary)
            hist_binned_primary->Fill(event->ipchi2);
        meanT += event->tau;
        i++;

        if (i % binSize == 0 || event == events.rend() - 1)
        {
            // Title the plot
            minT = (event - binSize < events.rbegin())?
                events.rbegin()->tau:
                (event - binSize)->tau;
            maxT = event->tau;
            title.Form("t = (%f,%f]", minT, maxT);
            hist_binned_primary->SetTitle(title);

            // Populate vectors
            meanTs.push_back(meanT / binSize);
            meanT = 0;

            // Draw histogram
            hist_binned_primary->Draw();
            // Save histogram
            binnedPrimaryCanvas->Print("BinnedPrimaryHists.pdf");

            // Empty histogram
            hist_binned_primary->Reset();

            // Clear Canvas
            binnedPrimaryCanvas->Clear();
        }
    }

    binnedPrimaryCanvas->Print("BinnedPrimaryHists.pdf]");
    binnedPrimaryCanvas->Close();
}

void MCanalysis::LifetimeBinnedSecondaryHists(const Int_t binSize)
{
    // Initialise the histogram, graphs, and canvas
    std::unique_ptr<TH1F> hist_binned_secondary{new TH1F{"hist_binned_secondary",
            "Binned Secondary", 100, MIN_IPCHI2, MAX_IPCHI2}};
    std::unique_ptr<TCanvas> binnedSecondaryCanvas{new TCanvas{"binnedSecondaryNoFit",
            "binnedSecondaryNoFit", 1440, 1080}};

    // Initialise vector to hold means
    std::vector<Double_t> meanTs;

    // Varaibles required to title the plots
    Double_t minT;
    Double_t maxT;
    Double_t meanT{0};
    TString title;

    // Open the pdf files
    binnedSecondaryCanvas->Print("BinnedSecondaryHists.pdf[");

    // Setup plots
    hist_binned_secondary->SetStats(kTRUE);
    hist_binned_secondary->SetLabelFont(133, "XYt");
    hist_binned_secondary->SetTitleFont(133);
    hist_binned_secondary->SetTitleFont(133, "XY");
    hist_binned_secondary->SetLabelSize(24, "XY");
    hist_binned_secondary->SetTitleSize(24, "XY");
    hist_binned_secondary->SetTitleOffset(1.05f, "X");
    hist_binned_secondary->SetTitleOffset(1.5, "Y");
    hist_binned_secondary->GetXaxis()->SetTitle("ln(#chi^{2}_{IP})");
    hist_binned_secondary->GetYaxis()->SetTitle("Entries");
    hist_binned_secondary->SetAxisRange(0, 2000, "Y");

    // Create a Secondary histogram for the Secondary values in every bin
    Int_t i{0};
    for(std::vector<Event>::reverse_iterator event = events.rbegin();
            event != events.rend(); event++)
    {
        if (event->type == Event::category::secondary)
            hist_binned_secondary->Fill(event->ipchi2);
        meanT += event->tau;
        i++;

        if (i % binSize == 0 || event == events.rend() - 1)
        {
            // Title the plot
            minT = (event - binSize < events.rbegin())?
                events.rbegin()->tau:
                (event - binSize)->tau;
            maxT = event->tau;
            title.Form("t = (%f,%f]", minT, maxT);
            hist_binned_secondary->SetTitle(title);

            // Populate vectors
            meanTs.push_back(meanT / binSize);
            meanT = 0;

            // Draw histogram
            hist_binned_secondary->Draw();
            // Save histogram
            binnedSecondaryCanvas->Print("BinnedSecondaryHists.pdf");

            // Empty histogram
            hist_binned_secondary->Reset();

            // Clear Canvas
            binnedSecondaryCanvas->Clear();
        }
    }

    binnedSecondaryCanvas->Print("BinnedSecondaryHists.pdf]");
    binnedSecondaryCanvas->Close();
}

void MCanalysis::MassHist()
{
    // create canvas for mass histogram
    std::unique_ptr<TCanvas> massCanvas{new TCanvas{"massNoFit", "massNoFit", 1440, 1080}};
    // draw histogram
    hist_mass->Draw();
    // save histogram
    massCanvas->SaveAs("InvariantMass.pdf");
    massCanvas->Close();
}

void MCanalysis::MassHistCut()
{
    // create canvas for mass histogram
    std::unique_ptr<TCanvas> massCanvas{new TCanvas{"massNoFit", "massNoFit", 1440, 1080}};
    // draw histogram
    hist_mass_cut->Draw();
    // save histogram
    massCanvas->SaveAs("InvariantMassCut.pdf");
    massCanvas->Close();
}

void MCanalysis::MassFit()
{
    // define parameter x
    RooRealVar x{"x", "Invariant Mass", 1830, 1900, "MeV c^-2"};
    // define mean of both Gaussians
    RooRealVar mean{"mean", "mean of Gaussian", 1.86481e3, 1860, 1870};
    // define sigma of first Gaussian
    RooRealVar sigma0{"sigma0", "sigma of gaussian0", 5.2, 4, 7};
    // define sigma of second Gaussian
    RooRealVar sigma1{"sigma1", "sigma of gaussian1", 9, 7, 10};

    // define decay constant for background exponential
    RooRealVar bkgd_exp_c1{"bkgd_exp_c1", "bkgd_exp_c1", -0.002, 0.};

    // define pdf for Gaussians using parameters above
    RooGaussian gauss0{"gauss0", "gaussian0 pdf peak", x, mean, sigma0};
    RooGaussian gauss1{"gauss1", "gaussian1 pdf peak", x, mean, sigma1};

    // define exponential background functions
    RooExponential bkgd_exp{"bkgd_exp", "background pdf peak", x, bkgd_exp_c1};

    // Define signal fractions
    Double_t maxYield {hist_mass->GetEntries()};
    RooRealVar gauss_yield0{"gauss_yield0", "gauss_yield0", 0, 1};
    // RooRealVar gauss_yield1{"gauss_yield1", "gauss_yield1", 0, maxYield};
    RooRealVar bkg_yield{"bkg_yield", "bkg_yield", 0, maxYield};

    // Define mass peak
    RooAddPdf peak{"peak", "mass peak", RooArgList(gauss0, gauss1),
        RooArgList(gauss_yield0)};
    RooRealVar peak_yield{"peak_yield", "peak_yield", 0, maxYield};

    // define argument list for pdfs
    RooArgList shapes;
    // define argument list for fractions of pdfs
    RooArgList yields;

    shapes.add(peak);
    yields.add(peak_yield);
    shapes.add(bkgd_exp);
    yields.add(bkg_yield);
    // shapes.add(gauss0);
    // yields.add(peak_yield0);
    // shapes.add(gauss1);
    // yields.add(peak_yield1);

    // Sum the components
    RooAddPdf sum{"sum", "gauss+backgd peak", shapes, yields};
    // Get data from histogram
    RooDataHist massData{"massData", "Invariant mass dataset peak", x,
        hist_mass.get()};
    // Fit data
    sum.fitTo(massData, Extended(kTRUE));

    // RooArgList peak;
    // peak.add(gauss0);
    // peak.add(gauss1);
    RooPlot* xframe{x.frame()};
    xframe->SetLabelFont(133, "XY");
    xframe->SetTitleFont(133, "XY");
    xframe->SetLabelSize(24, "XY");
    xframe->SetTitleSize(24, "XY");
    xframe->SetTitleOffset(1.3f, "X");
    xframe->SetTitleOffset(1.5, "Y");
    xframe->SetTitle("");
    xframe->SetXTitle("Invariant Mass (MeV/c^{2})");
    xframe->SetYTitle("Events");

    // plot data
    massData.plotOn(xframe, Name("massHistBins"));

    // plot Gaussian peak
    sum.plotOn(xframe, Components(bkgd_exp), LineStyle(7));
    sum.plotOn(xframe, Components(peak), LineColor(kRed));
    sum.plotOn(xframe, Components(gauss0), LineStyle(2), LineColor(kRed));
    sum.plotOn(xframe, Components(gauss1), LineStyle(2), LineColor(kRed));

    // plot total pdf
    sum.plotOn(xframe, Name("sum"));

    // Replot data
    massData.plotOn(xframe, Name("massHistBins"));

    // calculate chi squared
    cout << "CHI SQUARED IS " <<
        xframe->chiSquare("sum", "massHistBins", 5) << endl;

    // create pull plot
    RooHist *hpull{xframe->pullHist()};
    RooPlot *pullframe{x.frame()};
    pullframe->addPlotable(hpull, "EXB");
    pullframe->SetAxisRange(-4, 4, "Y");
    pullframe->SetLabelFont(133, "XY");
    pullframe->SetTitleFont(133, "XY");
    pullframe->SetLabelSize(24, "XY");
    pullframe->SetTitleSize(24, "XY");
    pullframe->SetTitleOffset(5, "X");
    pullframe->SetTickLength(numeric_cast<Float_t> (0.02 / 0.263), "X");
    pullframe->SetTickLength(0.02f, "Y");
    pullframe->SetTitle("");
    // pullframe->SetXTitle("ln(#chi^{2}_{IP})");
    pullframe->SetYTitle("Pull");

    // draw plots on a TCanvas
    std::unique_ptr<TCanvas> massFitCanvas{new TCanvas{"massFit", "massFit", 1440, 1080}};
    massFitCanvas->Divide(2);
    massFitCanvas->cd(1);
    gPad->SetPad(0.005, .24, 1., 1.);
    xframe->Draw();
    massFitCanvas->cd(2);
    gPad->SetPad(0.005 , 0.02, 1., .265);
    pullframe->Draw();
    massFitCanvas->SaveAs(TString("InvariantMassFit.pdf"));
}

void MCanalysis::MassFitCut()
{
    // define parameter x
    RooRealVar x{"x", "Invariant Mass", 1830, 1900, "MeV c^-2"};
    // define mean of both Gaussians
    RooRealVar mean{"mean", "mean of Gaussian", 1.86481e3, 1860, 1870};
    // define sigma of first Gaussian
    RooRealVar sigma0{"sigma0", "sigma of gaussian0", 5.2, 0.1, 7};
    // define sigma of second Gaussian
    RooRealVar sigma1{"sigma1", "sigma of gaussian1", 9, 7, 50};
    // define values for CB shape
    // RooRealVar alpha{"alpha", "alpha in CB shape", 0, 100};
    // RooRealVar n{"n", "n in CB shape", 0, 100};

    // define decay constant for background exponential
    RooRealVar bkgd_exp_c1{"bkgd_exp_c1", "bkgd_exp_c1", -0.002, 0.};

    // define pdf for Gaussians using parameters above
    RooGaussian gauss0{"gauss0", "gaussian0 pdf peak", x, mean, sigma0};
    // RooCBShape gauss0{"gauss0", "gaussian0 pdf peak", x, mean, sigma0,
    // alpha, n};
    RooGaussian gauss1{"gauss1", "gaussian1 pdf peak", x, mean, sigma1};

    // Define signal fractions
    Double_t maxYield{hist_mass_cut->GetEntries()};
    RooRealVar peak_yield0{"peak_yield0", "peak_yield0", 0, maxYield};
    RooRealVar peak_yield1{"peak_yield1", "peak_yield1", 0, maxYield};

    // define argument list for pdfs
    RooArgList shapes;
    // define argument list for fractions of pdfs
    RooArgList yields;
    shapes.add(gauss0);
    yields.add(peak_yield0);
    shapes.add(gauss1);
    yields.add(peak_yield1);

    // Sum the components
    RooAddPdf sum{"sum", "gauss+backgd peak", shapes, yields};
    // Get data from histogram
    RooDataHist massData{"massData", "Invariant mass dataset peak", x,
        hist_mass_cut.get()};
    // Fit data
    sum.fitTo(massData, Extended(true));

    RooArgList peak;
    peak.add(gauss0);
    peak.add(gauss1);
    RooPlot* xframe{x.frame()};
    xframe->SetLabelFont(133, "XY");
    xframe->SetTitleFont(133, "XY");
    xframe->SetLabelSize(24, "XY");
    xframe->SetTitleSize(24, "XY");
    xframe->SetTitleOffset(1.3f, "X");
    xframe->SetTitleOffset(1.5, "Y");
    xframe->SetTitle("");
    xframe->SetXTitle("Invariant Mass (MeV/c^{2})");
    xframe->SetYTitle("Events");

    // plot data
    massData.plotOn(xframe, Name("massHistBins"));

    // plot Gaussian peak
    sum.plotOn(xframe, Components(peak), LineColor(kRed));
    sum.plotOn(xframe, Components(gauss0), LineStyle(2), LineColor(kRed));
    sum.plotOn(xframe, Components(gauss1), LineStyle(2), LineColor(kRed));

    // plot total pdf
    sum.plotOn(xframe, Name("sum"));

    // Replot data
    massData.plotOn(xframe, Name("massHistBins"));

    // calculate chi squared
    cout << "CHI SQUARED IS " <<
        xframe->chiSquare("sum", "massHistBins", 5) << endl;

    // create pull plot
    RooHist *hpull{xframe->pullHist()};
    RooPlot *pullframe{x.frame()};
    pullframe->addPlotable(hpull, "EXB");
    pullframe->SetAxisRange(-4, 4, "Y");
    pullframe->SetLabelFont(133, "XY");
    pullframe->SetTitleFont(133, "XY");
    pullframe->SetLabelSize(24, "XY");
    pullframe->SetTitleSize(24, "XY");
    pullframe->SetTitleOffset(5, "X");
    pullframe->SetTickLength(numeric_cast<float> (0.02 / 0.263), "X");
    pullframe->SetTickLength(0.02f, "Y");
    pullframe->SetTitle("");
    // pullframe->SetXTitle("ln(#chi^{2}_{IP})");
    pullframe->SetYTitle("Pull");

    // draw plots on a TCanvas
    std::unique_ptr<TCanvas> massFitCanvas{new TCanvas{"massFit", "massFit", 1440, 1080}};
    massFitCanvas->Divide(2);
    massFitCanvas->cd(1);
    gPad->SetPad(0.005, .24, 1., 1.);
    xframe->Draw();
    massFitCanvas->cd(2);
    gPad->SetPad(0.005 , 0.02, 1., .265);
    pullframe->Draw();
    massFitCanvas->SaveAs(TString("InvariantMassFitCut.pdf"));
}

void MCanalysis::IPChi2Hist()
{
    std::unique_ptr<TCanvas> ipchi2Canvas{new TCanvas{"ipchi2NoFit", "ipchi2NoFit", 1440,
            1080}};
    // draw histogram
    hist_ipchi2->Draw();
    // save histogram
    ipchi2Canvas->SaveAs("LnIPChi2.pdf");
    ipchi2Canvas->Close();
}

void MCanalysis::IPChi2HistCut()
{
    std::unique_ptr<TCanvas> ipchi2Canvas{new TCanvas{"ipchi2NoFit", "ipchi2NoFit", 1440,
            1080}};
    // draw histogram
    hist_ipchi2_cut->Draw();
    // save histogram
    ipchi2Canvas->SaveAs("LnIPChi2Cut.pdf");
    ipchi2Canvas->Close();
}

void MCanalysis::IPChi2Fit()
{
    // Define parameter x
    RooRealVar x{"x", "Ln IP Chi2", -6, 10};
    // Define mean0 Gaussians
    RooRealVar mean0{"mean", "mean of Gaussians", 0.8726, 0.5, 2};
    RooRealVar mean1{"mean1", "mean of lone Gaussian", 6, 1, 6};
    // Define sigmas of Gaussians
    RooRealVar sigmal_0{"sigmal_0", "left  sigma of Gaussian0", 1, 0, 7};
    RooRealVar sigmar_0{"sigmar_0", "right sigma of Gaussian0", 1, 0, 7};
    RooRealVar sigmal_1{"sigmal_1", "left  sigma of Gaussian1", 1, 0, 7};
    RooRealVar sigmar_1{"sigmar_1", "right sigma of Gaussian1", 1, 0, 7};
    RooRealVar sigmal_2{"sigmal_2", "left  sigma of Gaussian2", 1, 0, 7};
    // RooRealVar sigmar_2{"sigmar_2", "right sigma of Gaussian2", 1, 0, 7};
    RooRealVar sigmal_3{"sigmal_3", "left  sigma of Gaussian3", 7, 1, 7};
    RooRealVar sigmar_3{"sigmar_3", "right sigma of Gaussian3", 1, 1, 7};

    // Define pdf for Gaussians using parameters above
    RooBifurGauss gauss0{"gauss0", "gaussian0", x, mean0, sigmal_0, sigmar_0};
    RooBifurGauss gauss1{"gauss1", "gaussian1", x, mean0, sigmal_1, sigmar_1};
    RooGaussian gauss2{"gauss2", "gaussian2", x, mean0, sigmal_2};
    RooBifurGauss gauss3{"gauss3", "gaussian3", x, mean1, sigmal_3, sigmar_3};

    // Define signal fractions
    Double_t maxYield{hist_ipchi2->GetEntries()};
    RooRealVar gauss_yield0{"gauss_yield0", "gauss_yield0", 0, maxYield};
    RooRealVar gauss_yield1{"gauss_yield1", "gauss_yield1", 0, maxYield};
    RooRealVar gauss_yield2{"gauss_yield2", "gauss_yield2", 0, maxYield};
    RooRealVar gauss_yield3{"gauss_yield3", "gauss_yield3", 0, maxYield};

    // Define primary peak
    RooAddPdf primary{"primary", "primary IPChi2 peak",
        RooArgList(gauss0, gauss1, gauss2),
        RooArgList(gauss_yield0, gauss_yield1, gauss_yield2)};
    RooRealVar primary_yield{"primary_yield", "primary_yield", 0, maxYield};

    // Define argument list for pdfs
    RooArgList shapes;
    shapes.add(primary);
    shapes.add(gauss3);

    // Define argument list for fractions of pdfs
    RooArgList yields;
    yields.add(primary_yield);
    yields.add(gauss_yield3);

    // Sum the components
    RooAddPdf sum{"sum", "Total ln ipchi2 pdf", shapes, yields};

    // Get data from histogram
    RooDataHist ipchi2Data{"ipchi2Data", "Ln ipchi2 dataset", x, hist_ipchi2.get()};

    // Fit data
    sum.fitTo(ipchi2Data, Extended(kTRUE));

    // Initialise plot
    RooPlot* xframe{x.frame()};
    xframe->SetLabelFont(133, "XY");
    xframe->SetTitleFont(133, "XY");
    xframe->SetLabelSize(24, "XY");
    xframe->SetTitleSize(24, "XY");
    xframe->SetTitleOffset(1.3f, "X");
    xframe->SetTitleOffset(1.5, "Y");
    xframe->SetTitle("");
    xframe->SetXTitle("ln(#chi^{2}_{IP})");
    xframe->SetYTitle("Events");

    // Plot data
    ipchi2Data.plotOn(xframe, Name("ipchi2HistBins"));

    // Plot pdf components
    sum.plotOn(xframe, Components(gauss0), LineStyle(2), LineColor(kGreen+1));
    sum.plotOn(xframe, Components(gauss1), LineStyle(2), LineColor(kGreen+1));
    sum.plotOn(xframe, Components(gauss2), LineStyle(3), LineColor(kGreen+4));
    sum.plotOn(xframe, Components(gauss3), LineStyle(2),
            LineColor(kMagenta+1));

    // plot total pdf
    sum.plotOn(xframe, Name("sum"));

    // Replot data
    ipchi2Data.plotOn(xframe, Name("ipchi2HistBins"));

    // calculate chi squared
    cout << "CHI SQUARED IS " <<
        xframe->chiSquare("sum", "ipchi2HistBins", 5) << endl;

    // create pull plot
    RooHist *hpull{xframe->pullHist()};
    RooPlot *pullframe{x.frame()};
    pullframe->addPlotable(hpull, "EXB");
    pullframe->SetAxisRange(-4, 4, "Y");
    pullframe->SetLabelFont(133, "XY");
    pullframe->SetTitleFont(133, "XY");
    pullframe->SetLabelSize(24, "XY");
    pullframe->SetTitleSize(24, "XY");
    pullframe->SetTitleOffset(5, "X");
    pullframe->SetTickLength(numeric_cast<Float_t> (0.02 / 0.263), "X");
    pullframe->SetTickLength(0.02f, "Y");
    pullframe->SetTitle("");
    // pullframe->SetXTitle("ln(#chi^{2}_{IP})");
    pullframe->SetYTitle("Pull");

    // draw plots on a TCanvas
    std::unique_ptr<TCanvas> ipchi2FitCanvas{new TCanvas{"ipchi2Fit", "ipchi2Fit", 1440,
            1080}};
    ipchi2FitCanvas->Divide(2);
    ipchi2FitCanvas->cd(1);
    gPad->SetPad(0.005, .24, 1., 1.);
    xframe->Draw();
    ipchi2FitCanvas->cd(2);
    gPad->SetPad(0.005 , 0.02, 1., .265);
    pullframe->Draw();
    ipchi2FitCanvas->SaveAs(TString("LnIPChi2Fit.pdf"));
}

void MCanalysis::IPChi2FitCut()
{
    // Define parameter x
    RooRealVar x{"x", "Ln IP Chi2", -6, 5};
    // Define mean Gaussians
    RooRealVar mean{"mean", "mean of Gaussians", 0.8726, 0.5, 2};
    // Define sigmas of Gaussians
    RooRealVar sigmal_0{"sigmal_0", "left  sigma of Gaussian0", 1, 0, 7};
    RooRealVar sigmar_0{"sigmar_0", "right sigma of Gaussian0", 1, 0, 7};
    RooRealVar sigmal_1{"sigmal_1", "left  sigma of Gaussian1", 1, 0, 7};
    RooRealVar sigmar_1{"sigmar_1", "right sigma of Gaussian1", 1, 0, 7};
    RooRealVar sigmal_2{"sigmal_2", "left  sigma of Gaussian2", 1, 0, 7};
    RooRealVar sigmar_2{"sigmar_2", "right sigma of Gaussian2", 1, 0, 7};

    // Define pdf for Gaussians using parameters above
    RooBifurGauss gauss0{"gauss0", "gaussian0", x, mean, sigmal_0, sigmar_0};
    RooBifurGauss gauss1{"gauss1", "gaussian1", x, mean, sigmal_1, sigmar_1};
    RooGaussian gauss2{"gauss2", "gaussian2", x, mean, sigmal_2};

    // Define signal fractions
    Double_t maxYield{hist_ipchi2_cut->GetEntries()};
    RooRealVar peak_yield0{"peak_yield0", "peak_yield0",  0, 0, maxYield};
    RooRealVar peak_yield1{"peak_yield1", "peak_yield1",  0, 0, maxYield};
    RooRealVar peak_yield2{"peak_yield2", "peak_yield2",  maxYield, 0,
        maxYield};

    // Define argument list for pdfs
    RooArgList shapes;
    shapes.add(gauss0);
    shapes.add(gauss1);
    shapes.add(gauss2);

    // Define argument list for fractions of pdfs
    RooArgList yields;
    yields.add(peak_yield0);
    yields.add(peak_yield1);
    yields.add(peak_yield2);

    // Sum the components
    RooAddPdf sum{"sum", "Total ln ipchi2 pdf", shapes, yields};

    // Get data from histogram
    RooDataHist ipchi2Data{"ipchi2Data", "Ln ipchi2 dataset", x, hist_ipchi2_cut.get()};

    // Fit data
    sum.fitTo(ipchi2Data, Extended(true));

    // Initialise plot
    RooPlot *xframe{x.frame()};
    xframe->SetLabelFont(133, "XY");
    xframe->SetTitleFont(133, "XY");
    xframe->SetLabelSize(24, "XY");
    xframe->SetTitleSize(24, "XY");
    xframe->SetTitleOffset(1.3f, "X");
    xframe->SetTitleOffset(1.5, "Y");
    xframe->SetTitle("");
    xframe->SetXTitle("ln(#chi^{2}_{IP})");
    xframe->SetYTitle("Events");

    // Plot data
    ipchi2Data.plotOn(xframe, Name("ipchi2HistBins"));

    // Plot pdf components
    sum.plotOn(xframe, Components(gauss0), LineStyle(2), LineColor(kGreen+1));
    sum.plotOn(xframe, Components(gauss1), LineStyle(2), LineColor(kGreen+1));
    sum.plotOn(xframe, Components(gauss2), LineStyle(3), LineColor(kGreen+4));

    // plot total pdf
    sum.plotOn(xframe, Name("sum"));

    // Replot data
    ipchi2Data.plotOn(xframe, Name("ipchi2HistBins"));

    // calculate chi squared
    cout << "CHI SQUARED IS " <<
        xframe->chiSquare("sum", "ipchi2HistBins", 5) << endl;

    // create pull plot
    RooHist *hpull{xframe->pullHist()};
    RooPlot *pullframe{x.frame()};
    pullframe->addPlotable(hpull, "EXB");
    pullframe->SetAxisRange(-4, 4, "Y");
    pullframe->SetLabelFont(133, "XY");
    pullframe->SetTitleFont(133, "XY");
    pullframe->SetLabelSize(24, "XY");
    pullframe->SetTitleSize(24, "XY");
    pullframe->SetTitleOffset(5, "X");
    pullframe->SetTickLength(numeric_cast<Float_t> (0.02 / 0.263), "X");
    pullframe->SetTickLength(0.02f, "Y");
    pullframe->SetTitle("");
    // pullframe->SetXTitle("ln(#chi^{2}_{IP})");
    pullframe->SetYTitle("Pull");

    // draw plots on a TCanvas
    std::unique_ptr<TCanvas> ipchi2FitCanvas{new TCanvas{"ipchi2Fit", "ipchi2Fit", 1440, 1080}};
    ipchi2FitCanvas->Divide(2);
    ipchi2FitCanvas->cd(1);
    gPad->SetPad(0.005, .24, 1., 1.);
    xframe->Draw();
    ipchi2FitCanvas->cd(2);
    gPad->SetPad(0.005 , 0.02, 1., .265);
    pullframe->Draw();
    ipchi2FitCanvas->SaveAs(TString("LnIPChi2FitCut.pdf"));
}

void MCanalysis::MassIPChi2Hist()
{
    std::unique_ptr<TCanvas> massipchi2Canvas{new TCanvas{"massipchi2NoFit", "massipchi2NoFit",
            1440, 1080}};
    // draw histogram
    hist_mass_ipchi2->Draw("lego");
    // save histogram
    massipchi2Canvas->SaveAs("MassIPChi2.pdf");
    massipchi2Canvas->Close();
}

void MCanalysis::Terminate()
{
    // The Terminate() function is the last function to be called during
    // a query. It always runs on the client, it can be used to present
    // the results graphically or save the results to file.

    // TCanvas *trueidCanvas = new TCanvas("trueidNoFit", "trueidNoFit");
    // // draw histogram
    // hist_trueid->Draw();
    // // save histogram
    // trueidCanvas->SaveAs("TRUEID.pdf");
    // trueidCanvas->Close();
    // delete trueidCanvas;
    // trueidCanvas = nullptr;

    // MASS PLOTS
    MassHist();
    MassFit();
    MassHistCut();
    MassFitCut();

    // IP CHI2 PLOTS
    IPChi2Hist();
    IPChi2Fit();
    IPChi2HistCut();
    IPChi2FitCut();

    // MASS + IP CHI2 PLOTS
    MassIPChi2Hist();

    // LIFETIME PLOTS
    TauHist();

    // EQUAL ENTRY LIFETIME BINS
    Int_t binSize{EqualLifetimeBins()};
    LifetimeBinnedMassHists(binSize);
    LifetimeBinnedIPChi2Hists(binSize);
    LifetimeBinnedThetaHists(binSize);
    LifetimeBinnedPrimaryHists(binSize);
    LifetimeBinnedSecondaryHists(binSize);
}

void MCanalysis::Init(TTree *tree)
{
    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).

    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fChain->SetMakeClass(1);

    fChain->SetBranchAddress("lab0_MINIP", &lab0_MINIP, &b_lab0_MINIP);
    fChain->SetBranchAddress("lab0_MINIPCHI2", &lab0_MINIPCHI2, &b_lab0_MINIPCHI2);
    fChain->SetBranchAddress("lab0_MINIPNEXTBEST", &lab0_MINIPNEXTBEST, &b_lab0_MINIPNEXTBEST);
    fChain->SetBranchAddress("lab0_MINIPCHI2NEXTBEST", &lab0_MINIPCHI2NEXTBEST, &b_lab0_MINIPCHI2NEXTBEST);
    fChain->SetBranchAddress("lab0_ENDVERTEX_X", &lab0_ENDVERTEX_X, &b_lab0_ENDVERTEX_X);
    fChain->SetBranchAddress("lab0_ENDVERTEX_Y", &lab0_ENDVERTEX_Y, &b_lab0_ENDVERTEX_Y);
    fChain->SetBranchAddress("lab0_ENDVERTEX_Z", &lab0_ENDVERTEX_Z, &b_lab0_ENDVERTEX_Z);
    fChain->SetBranchAddress("lab0_ENDVERTEX_XERR", &lab0_ENDVERTEX_XERR, &b_lab0_ENDVERTEX_XERR);
    fChain->SetBranchAddress("lab0_ENDVERTEX_YERR", &lab0_ENDVERTEX_YERR, &b_lab0_ENDVERTEX_YERR);
    fChain->SetBranchAddress("lab0_ENDVERTEX_ZERR", &lab0_ENDVERTEX_ZERR, &b_lab0_ENDVERTEX_ZERR);
    fChain->SetBranchAddress("lab0_ENDVERTEX_CHI2", &lab0_ENDVERTEX_CHI2, &b_lab0_ENDVERTEX_CHI2);
    fChain->SetBranchAddress("lab0_ENDVERTEX_NDOF", &lab0_ENDVERTEX_NDOF, &b_lab0_ENDVERTEX_NDOF);
    fChain->SetBranchAddress("lab0_OWNPV_Y", &lab0_OWNPV_Y, &b_lab0_OWNPV_Y);
    fChain->SetBranchAddress("lab0_OWNPV_Z", &lab0_OWNPV_Z, &b_lab0_OWNPV_Z);
    fChain->SetBranchAddress("lab0_OWNPV_XERR", &lab0_OWNPV_XERR, &b_lab0_OWNPV_XERR);
    fChain->SetBranchAddress("lab0_OWNPV_YERR", &lab0_OWNPV_YERR, &b_lab0_OWNPV_YERR);
    fChain->SetBranchAddress("lab0_OWNPV_ZERR", &lab0_OWNPV_ZERR, &b_lab0_OWNPV_ZERR);
    fChain->SetBranchAddress("lab0_OWNPV_CHI2", &lab0_OWNPV_CHI2, &b_lab0_OWNPV_CHI2);
    fChain->SetBranchAddress("lab0_OWNPV_NDOF", &lab0_OWNPV_NDOF, &b_lab0_OWNPV_NDOF);
    fChain->SetBranchAddress("lab0_OWNPV_COV_", lab0_OWNPV_COV_, &b_lab0_OWNPV_COV_);
    fChain->SetBranchAddress("lab0_IP_OWNPV", &lab0_IP_OWNPV, &b_lab0_IP_OWNPV);
    fChain->SetBranchAddress("lab0_IPCHI2_OWNPV", &lab0_IPCHI2_OWNPV, &b_lab0_IPCHI2_OWNPV);
    fChain->SetBranchAddress("lab0_FD_OWNPV", &lab0_FD_OWNPV, &b_lab0_FD_OWNPV);
    fChain->SetBranchAddress("lab0_FDCHI2_OWNPV", &lab0_FDCHI2_OWNPV, &b_lab0_FDCHI2_OWNPV);
    fChain->SetBranchAddress("lab0_DIRA_OWNPV", &lab0_DIRA_OWNPV, &b_lab0_DIRA_OWNPV);
    fChain->SetBranchAddress("lab0_TOPPV_X", &lab0_TOPPV_X, &b_lab0_TOPPV_X);
    fChain->SetBranchAddress("lab0_TOPPV_Y", &lab0_TOPPV_Y, &b_lab0_TOPPV_Y);
    fChain->SetBranchAddress("lab0_TOPPV_Z", &lab0_TOPPV_Z, &b_lab0_TOPPV_Z);
    fChain->SetBranchAddress("lab0_TOPPV_XERR", &lab0_TOPPV_XERR, &b_lab0_TOPPV_XERR);
    fChain->SetBranchAddress("lab0_TOPPV_YERR", &lab0_TOPPV_YERR, &b_lab0_TOPPV_YERR);
    fChain->SetBranchAddress("lab0_TOPPV_ZERR", &lab0_TOPPV_ZERR, &b_lab0_TOPPV_ZERR);
    fChain->SetBranchAddress("lab0_TOPPV_CHI2", &lab0_TOPPV_CHI2, &b_lab0_TOPPV_CHI2);
    fChain->SetBranchAddress("lab0_TOPPV_NDOF", &lab0_TOPPV_NDOF, &b_lab0_TOPPV_NDOF);
    fChain->SetBranchAddress("lab0_TOPPV_COV_", lab0_TOPPV_COV_, &b_lab0_TOPPV_COV_);
    fChain->SetBranchAddress("lab0_IP_TOPPV", &lab0_IP_TOPPV, &b_lab0_IP_TOPPV);
    fChain->SetBranchAddress("lab0_IPCHI2_TOPPV", &lab0_IPCHI2_TOPPV, &b_lab0_IPCHI2_TOPPV);
    fChain->SetBranchAddress("lab0_FD_TOPPV", &lab0_FD_TOPPV, &b_lab0_FD_TOPPV);
    fChain->SetBranchAddress("lab0_FDCHI2_TOPPV", &lab0_FDCHI2_TOPPV, &b_lab0_FDCHI2_TOPPV);
    fChain->SetBranchAddress("lab0_DIRA_TOPPV", &lab0_DIRA_TOPPV, &b_lab0_DIRA_TOPPV);
    fChain->SetBranchAddress("lab0_P", &lab0_P, &b_lab0_P);
    fChain->SetBranchAddress("lab0_PT", &lab0_PT, &b_lab0_PT);
    fChain->SetBranchAddress("lab0_PE", &lab0_PE, &b_lab0_PE);
    fChain->SetBranchAddress("lab0_PX", &lab0_PX, &b_lab0_PX);
    fChain->SetBranchAddress("lab0_PY", &lab0_PY, &b_lab0_PY);
    fChain->SetBranchAddress("lab0_PZ", &lab0_PZ, &b_lab0_PZ);
    fChain->SetBranchAddress("lab0_MM", &lab0_MM, &b_lab0_MM);
    fChain->SetBranchAddress("lab0_MMERR", &lab0_MMERR, &b_lab0_MMERR);
    fChain->SetBranchAddress("lab0_M", &lab0_M, &b_lab0_M);
    fChain->SetBranchAddress("lab0_BKGCAT", &lab0_BKGCAT, &b_lab0_BKGCAT);
    fChain->SetBranchAddress("lab0_TRUEID", &lab0_TRUEID, &b_lab0_TRUEID);
    fChain->SetBranchAddress("lab0_MC_MOTHER_ID", &lab0_MC_MOTHER_ID, &b_lab0_MC_MOTHER_ID);
    fChain->SetBranchAddress("lab0_MC_MOTHER_KEY", &lab0_MC_MOTHER_KEY, &b_lab0_MC_MOTHER_KEY);
    fChain->SetBranchAddress("lab0_MC_GD_MOTHER_ID", &lab0_MC_GD_MOTHER_ID, &b_lab0_MC_GD_MOTHER_ID);
    fChain->SetBranchAddress("lab0_MC_GD_MOTHER_KEY", &lab0_MC_GD_MOTHER_KEY, &b_lab0_MC_GD_MOTHER_KEY);
    fChain->SetBranchAddress("lab0_MC_GD_GD_MOTHER_ID", &lab0_MC_GD_GD_MOTHER_ID, &b_lab0_MC_GD_GD_MOTHER_ID);
    fChain->SetBranchAddress("lab0_MC_GD_GD_MOTHER_KEY", &lab0_MC_GD_GD_MOTHER_KEY, &b_lab0_MC_GD_GD_MOTHER_KEY);
    fChain->SetBranchAddress("lab0_TRUEP_E", &lab0_TRUEP_E, &b_lab0_TRUEP_E);
    fChain->SetBranchAddress("lab0_TRUEP_X", &lab0_TRUEP_X, &b_lab0_TRUEP_X);
    fChain->SetBranchAddress("lab0_TRUEP_Y", &lab0_TRUEP_Y, &b_lab0_TRUEP_Y);
    fChain->SetBranchAddress("lab0_TRUEP_Z", &lab0_TRUEP_Z, &b_lab0_TRUEP_Z);
    fChain->SetBranchAddress("lab0_TRUEPT", &lab0_TRUEPT, &b_lab0_TRUEPT);
    fChain->SetBranchAddress("lab0_TRUEORIGINVERTEX_X", &lab0_TRUEORIGINVERTEX_X, &b_lab0_TRUEORIGINVERTEX_X);
    fChain->SetBranchAddress("lab0_TRUEORIGINVERTEX_Y", &lab0_TRUEORIGINVERTEX_Y, &b_lab0_TRUEORIGINVERTEX_Y);
    fChain->SetBranchAddress("lab0_TRUEORIGINVERTEX_Z", &lab0_TRUEORIGINVERTEX_Z, &b_lab0_TRUEORIGINVERTEX_Z);
    fChain->SetBranchAddress("lab0_TRUEENDVERTEX_X", &lab0_TRUEENDVERTEX_X, &b_lab0_TRUEENDVERTEX_X);
    fChain->SetBranchAddress("lab0_TRUEENDVERTEX_Y", &lab0_TRUEENDVERTEX_Y, &b_lab0_TRUEENDVERTEX_Y);
    fChain->SetBranchAddress("lab0_TRUEENDVERTEX_Z", &lab0_TRUEENDVERTEX_Z, &b_lab0_TRUEENDVERTEX_Z);
    fChain->SetBranchAddress("lab0_TRUEISSTABLE", &lab0_TRUEISSTABLE, &b_lab0_TRUEISSTABLE);
    fChain->SetBranchAddress("lab0_TRUETAU", &lab0_TRUETAU, &b_lab0_TRUETAU);
    fChain->SetBranchAddress("lab0_ID", &lab0_ID, &b_lab0_ID);
    fChain->SetBranchAddress("lab0_TAU", &lab0_TAU, &b_lab0_TAU);
    fChain->SetBranchAddress("lab0_TAUERR", &lab0_TAUERR, &b_lab0_TAUERR);
    fChain->SetBranchAddress("lab0_TAUCHI2", &lab0_TAUCHI2, &b_lab0_TAUCHI2);
    fChain->SetBranchAddress("lab0_L0Global_Dec", &lab0_L0Global_Dec, &b_lab0_L0Global_Dec);
    fChain->SetBranchAddress("lab0_L0Global_TIS", &lab0_L0Global_TIS, &b_lab0_L0Global_TIS);
    fChain->SetBranchAddress("lab0_L0Global_TOS", &lab0_L0Global_TOS, &b_lab0_L0Global_TOS);
    fChain->SetBranchAddress("lab0_Hlt1Global_Dec", &lab0_Hlt1Global_Dec, &b_lab0_Hlt1Global_Dec);
    fChain->SetBranchAddress("lab0_Hlt1Global_TIS", &lab0_Hlt1Global_TIS, &b_lab0_Hlt1Global_TIS);
    fChain->SetBranchAddress("lab0_Hlt1Global_TOS", &lab0_Hlt1Global_TOS, &b_lab0_Hlt1Global_TOS);
    fChain->SetBranchAddress("lab0_Hlt1Phys_Dec", &lab0_Hlt1Phys_Dec, &b_lab0_Hlt1Phys_Dec);
    fChain->SetBranchAddress("lab0_Hlt1Phys_TIS", &lab0_Hlt1Phys_TIS, &b_lab0_Hlt1Phys_TIS);
    fChain->SetBranchAddress("lab0_Hlt1Phys_TOS", &lab0_Hlt1Phys_TOS, &b_lab0_Hlt1Phys_TOS);
    fChain->SetBranchAddress("lab0_Hlt2Global_Dec", &lab0_Hlt2Global_Dec, &b_lab0_Hlt2Global_Dec);
    fChain->SetBranchAddress("lab0_Hlt2Global_TIS", &lab0_Hlt2Global_TIS, &b_lab0_Hlt2Global_TIS);
    fChain->SetBranchAddress("lab0_Hlt2Global_TOS", &lab0_Hlt2Global_TOS, &b_lab0_Hlt2Global_TOS);
    fChain->SetBranchAddress("lab0_Hlt2Phys_Dec", &lab0_Hlt2Phys_Dec, &b_lab0_Hlt2Phys_Dec);
    fChain->SetBranchAddress("lab1_MINIP", &lab1_MINIP, &b_lab1_MINIP);
    fChain->SetBranchAddress("lab1_MINIPCHI2", &lab1_MINIPCHI2, &b_lab1_MINIPCHI2);
    fChain->SetBranchAddress("lab1_MINIPNEXTBEST", &lab1_MINIPNEXTBEST, &b_lab1_MINIPNEXTBEST);
    fChain->SetBranchAddress("lab1_MINIPCHI2NEXTBEST", &lab1_MINIPCHI2NEXTBEST, &b_lab1_MINIPCHI2NEXTBEST);
    fChain->SetBranchAddress("lab1_OWNPV_X", &lab1_OWNPV_X, &b_lab1_OWNPV_X);
    fChain->SetBranchAddress("lab1_OWNPV_Y", &lab1_OWNPV_Y, &b_lab1_OWNPV_Y);
    fChain->SetBranchAddress("lab1_OWNPV_Z", &lab1_OWNPV_Z, &b_lab1_OWNPV_Z);
    fChain->SetBranchAddress("lab1_OWNPV_XERR", &lab1_OWNPV_XERR, &b_lab1_OWNPV_XERR);
    fChain->SetBranchAddress("lab1_OWNPV_YERR", &lab1_OWNPV_YERR, &b_lab1_OWNPV_YERR);
    fChain->SetBranchAddress("lab1_OWNPV_ZERR", &lab1_OWNPV_ZERR, &b_lab1_OWNPV_ZERR);
    fChain->SetBranchAddress("lab1_OWNPV_CHI2", &lab1_OWNPV_CHI2, &b_lab1_OWNPV_CHI2);
    fChain->SetBranchAddress("lab1_OWNPV_NDOF", &lab1_OWNPV_NDOF, &b_lab1_OWNPV_NDOF);
    fChain->SetBranchAddress("lab1_OWNPV_COV_", lab1_OWNPV_COV_, &b_lab1_OWNPV_COV_);
    fChain->SetBranchAddress("lab1_IP_OWNPV", &lab1_IP_OWNPV, &b_lab1_IP_OWNPV);
    fChain->SetBranchAddress("lab1_IPCHI2_OWNPV", &lab1_IPCHI2_OWNPV, &b_lab1_IPCHI2_OWNPV);
    fChain->SetBranchAddress("lab1_TOPPV_X", &lab1_TOPPV_X, &b_lab1_TOPPV_X);
    fChain->SetBranchAddress("lab1_TOPPV_Y", &lab1_TOPPV_Y, &b_lab1_TOPPV_Y);
    fChain->SetBranchAddress("lab1_TOPPV_Z", &lab1_TOPPV_Z, &b_lab1_TOPPV_Z);
    fChain->SetBranchAddress("lab1_TOPPV_XERR", &lab1_TOPPV_XERR, &b_lab1_TOPPV_XERR);
    fChain->SetBranchAddress("lab1_TOPPV_YERR", &lab1_TOPPV_YERR, &b_lab1_TOPPV_YERR);
    fChain->SetBranchAddress("lab1_TOPPV_ZERR", &lab1_TOPPV_ZERR, &b_lab1_TOPPV_ZERR);
    fChain->SetBranchAddress("lab1_TOPPV_CHI2", &lab1_TOPPV_CHI2, &b_lab1_TOPPV_CHI2);
    fChain->SetBranchAddress("lab1_TOPPV_NDOF", &lab1_TOPPV_NDOF, &b_lab1_TOPPV_NDOF);
    fChain->SetBranchAddress("lab1_TOPPV_COV_", lab1_TOPPV_COV_, &b_lab1_TOPPV_COV_);
    fChain->SetBranchAddress("lab1_IP_TOPPV", &lab1_IP_TOPPV, &b_lab1_IP_TOPPV);
    fChain->SetBranchAddress("lab1_IPCHI2_TOPPV", &lab1_IPCHI2_TOPPV, &b_lab1_IPCHI2_TOPPV);
    fChain->SetBranchAddress("lab1_ORIVX_X", &lab1_ORIVX_X, &b_lab1_ORIVX_X);
    fChain->SetBranchAddress("lab1_ORIVX_Y", &lab1_ORIVX_Y, &b_lab1_ORIVX_Y);
    fChain->SetBranchAddress("lab1_ORIVX_Z", &lab1_ORIVX_Z, &b_lab1_ORIVX_Z);
    fChain->SetBranchAddress("lab1_ORIVX_XERR", &lab1_ORIVX_XERR, &b_lab1_ORIVX_XERR);
    fChain->SetBranchAddress("lab1_ORIVX_YERR", &lab1_ORIVX_YERR, &b_lab1_ORIVX_YERR);
    fChain->SetBranchAddress("lab1_ORIVX_ZERR", &lab1_ORIVX_ZERR, &b_lab1_ORIVX_ZERR);
    fChain->SetBranchAddress("lab1_ORIVX_CHI2", &lab1_ORIVX_CHI2, &b_lab1_ORIVX_CHI2);
    fChain->SetBranchAddress("lab1_ORIVX_NDOF", &lab1_ORIVX_NDOF, &b_lab1_ORIVX_NDOF);
    fChain->SetBranchAddress("lab1_ORIVX_COV_", lab1_ORIVX_COV_, &b_lab1_ORIVX_COV_);
    fChain->SetBranchAddress("lab1_IP_ORIVX", &lab1_IP_ORIVX, &b_lab1_IP_ORIVX);
    fChain->SetBranchAddress("lab1_IPCHI2_ORIVX", &lab1_IPCHI2_ORIVX, &b_lab1_IPCHI2_ORIVX);
    fChain->SetBranchAddress("lab1_P", &lab1_P, &b_lab1_P);
    fChain->SetBranchAddress("lab1_PT", &lab1_PT, &b_lab1_PT);
    fChain->SetBranchAddress("lab1_PE", &lab1_PE, &b_lab1_PE);
    fChain->SetBranchAddress("lab1_PX", &lab1_PX, &b_lab1_PX);
    fChain->SetBranchAddress("lab1_PY", &lab1_PY, &b_lab1_PY);
    fChain->SetBranchAddress("lab1_PZ", &lab1_PZ, &b_lab1_PZ);
    fChain->SetBranchAddress("lab1_M", &lab1_M, &b_lab1_M);
    fChain->SetBranchAddress("lab1_TRUEID", &lab1_TRUEID, &b_lab1_TRUEID);
    fChain->SetBranchAddress("lab1_MC_MOTHER_ID", &lab1_MC_MOTHER_ID, &b_lab1_MC_MOTHER_ID);
    fChain->SetBranchAddress("lab1_MC_MOTHER_KEY", &lab1_MC_MOTHER_KEY, &b_lab1_MC_MOTHER_KEY);
    fChain->SetBranchAddress("lab1_MC_GD_MOTHER_ID", &lab1_MC_GD_MOTHER_ID, &b_lab1_MC_GD_MOTHER_ID);
    fChain->SetBranchAddress("lab1_MC_GD_MOTHER_KEY", &lab1_MC_GD_MOTHER_KEY, &b_lab1_MC_GD_MOTHER_KEY);
    fChain->SetBranchAddress("lab1_MC_GD_GD_MOTHER_ID", &lab1_MC_GD_GD_MOTHER_ID, &b_lab1_MC_GD_GD_MOTHER_ID);
    fChain->SetBranchAddress("lab1_MC_GD_GD_MOTHER_KEY", &lab1_MC_GD_GD_MOTHER_KEY, &b_lab1_MC_GD_GD_MOTHER_KEY);
    fChain->SetBranchAddress("lab1_TRUEP_E", &lab1_TRUEP_E, &b_lab1_TRUEP_E);
    fChain->SetBranchAddress("lab1_TRUEP_X", &lab1_TRUEP_X, &b_lab1_TRUEP_X);
    fChain->SetBranchAddress("lab1_TRUEP_Y", &lab1_TRUEP_Y, &b_lab1_TRUEP_Y);
    fChain->SetBranchAddress("lab1_TRUEP_Z", &lab1_TRUEP_Z, &b_lab1_TRUEP_Z);
    fChain->SetBranchAddress("lab1_TRUEPT", &lab1_TRUEPT, &b_lab1_TRUEPT);
    fChain->SetBranchAddress("lab1_TRUEORIGINVERTEX_X", &lab1_TRUEORIGINVERTEX_X, &b_lab1_TRUEORIGINVERTEX_X);
    fChain->SetBranchAddress("lab1_TRUEORIGINVERTEX_Y", &lab1_TRUEORIGINVERTEX_Y, &b_lab1_TRUEORIGINVERTEX_Y);
    fChain->SetBranchAddress("lab1_TRUEORIGINVERTEX_Z", &lab1_TRUEORIGINVERTEX_Z, &b_lab1_TRUEORIGINVERTEX_Z);
    fChain->SetBranchAddress("lab1_TRUEENDVERTEX_X", &lab1_TRUEENDVERTEX_X, &b_lab1_TRUEENDVERTEX_X);
    fChain->SetBranchAddress("lab1_TRUEENDVERTEX_Y", &lab1_TRUEENDVERTEX_Y, &b_lab1_TRUEENDVERTEX_Y);
    fChain->SetBranchAddress("lab1_TRUEENDVERTEX_Z", &lab1_TRUEENDVERTEX_Z, &b_lab1_TRUEENDVERTEX_Z);
    fChain->SetBranchAddress("lab1_TRUEISSTABLE", &lab1_TRUEISSTABLE, &b_lab1_TRUEISSTABLE);
    fChain->SetBranchAddress("lab1_TRUETAU", &lab1_TRUETAU, &b_lab1_TRUETAU);
    fChain->SetBranchAddress("lab1_ID", &lab1_ID, &b_lab1_ID);
    fChain->SetBranchAddress("lab1_PIDe", &lab1_PIDe, &b_lab1_PIDe);
    fChain->SetBranchAddress("lab1_PIDmu", &lab1_PIDmu, &b_lab1_PIDmu);
    fChain->SetBranchAddress("lab1_PIDK", &lab1_PIDK, &b_lab1_PIDK);
    fChain->SetBranchAddress("lab1_PIDp", &lab1_PIDp, &b_lab1_PIDp);
    fChain->SetBranchAddress("lab1_ProbNNe", &lab1_ProbNNe, &b_lab1_ProbNNe);
    fChain->SetBranchAddress("lab1_ProbNNk", &lab1_ProbNNk, &b_lab1_ProbNNk);
    fChain->SetBranchAddress("lab1_ProbNNp", &lab1_ProbNNp, &b_lab1_ProbNNp);
    fChain->SetBranchAddress("lab1_ProbNNpi", &lab1_ProbNNpi, &b_lab1_ProbNNpi);
    fChain->SetBranchAddress("lab1_ProbNNmu", &lab1_ProbNNmu, &b_lab1_ProbNNmu);
    fChain->SetBranchAddress("lab1_ProbNNghost", &lab1_ProbNNghost, &b_lab1_ProbNNghost);
    fChain->SetBranchAddress("lab1_hasMuon", &lab1_hasMuon, &b_lab1_hasMuon);
    fChain->SetBranchAddress("lab1_isMuon", &lab1_isMuon, &b_lab1_isMuon);
    fChain->SetBranchAddress("lab1_hasRich", &lab1_hasRich, &b_lab1_hasRich);
    fChain->SetBranchAddress("lab1_hasCalo", &lab1_hasCalo, &b_lab1_hasCalo);
    fChain->SetBranchAddress("lab1_L0Global_Dec", &lab1_L0Global_Dec, &b_lab1_L0Global_Dec);
    fChain->SetBranchAddress("lab1_L0Global_TIS", &lab1_L0Global_TIS, &b_lab1_L0Global_TIS);
    fChain->SetBranchAddress("lab1_L0Global_TOS", &lab1_L0Global_TOS, &b_lab1_L0Global_TOS);
    fChain->SetBranchAddress("lab1_Hlt1Global_Dec", &lab1_Hlt1Global_Dec, &b_lab1_Hlt1Global_Dec);
    fChain->SetBranchAddress("lab1_Hlt1Global_TIS", &lab1_Hlt1Global_TIS, &b_lab1_Hlt1Global_TIS);
    fChain->SetBranchAddress("lab1_Hlt1Global_TOS", &lab1_Hlt1Global_TOS, &b_lab1_Hlt1Global_TOS);
    fChain->SetBranchAddress("lab1_Hlt1Phys_Dec", &lab1_Hlt1Phys_Dec, &b_lab1_Hlt1Phys_Dec);
    fChain->SetBranchAddress("lab1_Hlt1Phys_TIS", &lab1_Hlt1Phys_TIS, &b_lab1_Hlt1Phys_TIS);
    fChain->SetBranchAddress("lab1_Hlt1Phys_TOS", &lab1_Hlt1Phys_TOS, &b_lab1_Hlt1Phys_TOS);
    fChain->SetBranchAddress("lab1_Hlt2Global_Dec", &lab1_Hlt2Global_Dec, &b_lab1_Hlt2Global_Dec);
    fChain->SetBranchAddress("lab1_Hlt2Global_TIS", &lab1_Hlt2Global_TIS, &b_lab1_Hlt2Global_TIS);
    fChain->SetBranchAddress("lab1_Hlt2Global_TOS", &lab1_Hlt2Global_TOS, &b_lab1_Hlt2Global_TOS);
    fChain->SetBranchAddress("lab1_Hlt2Phys_Dec", &lab1_Hlt2Phys_Dec, &b_lab1_Hlt2Phys_Dec);
    fChain->SetBranchAddress("lab1_TRACK_Type", &lab1_TRACK_Type, &b_lab1_TRACK_Type);
    fChain->SetBranchAddress("lab1_TRACK_Key", &lab1_TRACK_Key, &b_lab1_TRACK_Key);
    fChain->SetBranchAddress("lab1_TRACK_CHI2NDOF", &lab1_TRACK_CHI2NDOF, &b_lab1_TRACK_CHI2NDOF);
    fChain->SetBranchAddress("lab1_TRACK_PCHI2", &lab1_TRACK_PCHI2, &b_lab1_TRACK_PCHI2);
    fChain->SetBranchAddress("lab1_TRACK_MatchCHI2", &lab1_TRACK_MatchCHI2, &b_lab1_TRACK_MatchCHI2);
    fChain->SetBranchAddress("lab1_TRACK_GhostProb", &lab1_TRACK_GhostProb, &b_lab1_TRACK_GhostProb);
    fChain->SetBranchAddress("lab1_TRACK_CloneDist", &lab1_TRACK_CloneDist, &b_lab1_TRACK_CloneDist);
    fChain->SetBranchAddress("lab1_TRACK_Likelihood", &lab1_TRACK_Likelihood, &b_lab1_TRACK_Likelihood);
    fChain->SetBranchAddress("lab2_MINIP", &lab2_MINIP, &b_lab2_MINIP);
    fChain->SetBranchAddress("lab2_MINIPCHI2", &lab2_MINIPCHI2, &b_lab2_MINIPCHI2);
    fChain->SetBranchAddress("lab2_MINIPNEXTBEST", &lab2_MINIPNEXTBEST, &b_lab2_MINIPNEXTBEST);
    fChain->SetBranchAddress("lab2_MINIPCHI2NEXTBEST", &lab2_MINIPCHI2NEXTBEST, &b_lab2_MINIPCHI2NEXTBEST);
    fChain->SetBranchAddress("lab2_OWNPV_X", &lab2_OWNPV_X, &b_lab2_OWNPV_X);
    fChain->SetBranchAddress("lab2_OWNPV_Y", &lab2_OWNPV_Y, &b_lab2_OWNPV_Y);
    fChain->SetBranchAddress("lab2_OWNPV_Z", &lab2_OWNPV_Z, &b_lab2_OWNPV_Z);
    fChain->SetBranchAddress("lab2_OWNPV_XERR", &lab2_OWNPV_XERR, &b_lab2_OWNPV_XERR);
    fChain->SetBranchAddress("lab2_OWNPV_YERR", &lab2_OWNPV_YERR, &b_lab2_OWNPV_YERR);
    fChain->SetBranchAddress("lab2_OWNPV_ZERR", &lab2_OWNPV_ZERR, &b_lab2_OWNPV_ZERR);
    fChain->SetBranchAddress("lab2_OWNPV_CHI2", &lab2_OWNPV_CHI2, &b_lab2_OWNPV_CHI2);
    fChain->SetBranchAddress("lab2_OWNPV_NDOF", &lab2_OWNPV_NDOF, &b_lab2_OWNPV_NDOF);
    fChain->SetBranchAddress("lab2_OWNPV_COV_", lab2_OWNPV_COV_, &b_lab2_OWNPV_COV_);
    fChain->SetBranchAddress("lab2_IP_OWNPV", &lab2_IP_OWNPV, &b_lab2_IP_OWNPV);
    fChain->SetBranchAddress("lab2_IPCHI2_OWNPV", &lab2_IPCHI2_OWNPV, &b_lab2_IPCHI2_OWNPV);
    fChain->SetBranchAddress("lab2_TOPPV_X", &lab2_TOPPV_X, &b_lab2_TOPPV_X);
    fChain->SetBranchAddress("lab2_TOPPV_Y", &lab2_TOPPV_Y, &b_lab2_TOPPV_Y);
    fChain->SetBranchAddress("lab2_TOPPV_Z", &lab2_TOPPV_Z, &b_lab2_TOPPV_Z);
    fChain->SetBranchAddress("lab2_TOPPV_XERR", &lab2_TOPPV_XERR, &b_lab2_TOPPV_XERR);
    fChain->SetBranchAddress("lab2_TOPPV_YERR", &lab2_TOPPV_YERR, &b_lab2_TOPPV_YERR);
    fChain->SetBranchAddress("lab2_TOPPV_ZERR", &lab2_TOPPV_ZERR, &b_lab2_TOPPV_ZERR);
    fChain->SetBranchAddress("lab2_TOPPV_CHI2", &lab2_TOPPV_CHI2, &b_lab2_TOPPV_CHI2);
    fChain->SetBranchAddress("lab2_TOPPV_NDOF", &lab2_TOPPV_NDOF, &b_lab2_TOPPV_NDOF);
    fChain->SetBranchAddress("lab2_TOPPV_COV_", lab2_TOPPV_COV_, &b_lab2_TOPPV_COV_);
    fChain->SetBranchAddress("lab2_IP_TOPPV", &lab2_IP_TOPPV, &b_lab2_IP_TOPPV);
    fChain->SetBranchAddress("lab2_IPCHI2_TOPPV", &lab2_IPCHI2_TOPPV, &b_lab2_IPCHI2_TOPPV);
    fChain->SetBranchAddress("lab2_ORIVX_X", &lab2_ORIVX_X, &b_lab2_ORIVX_X);
    fChain->SetBranchAddress("lab2_ORIVX_Y", &lab2_ORIVX_Y, &b_lab2_ORIVX_Y);
    fChain->SetBranchAddress("lab2_ORIVX_Z", &lab2_ORIVX_Z, &b_lab2_ORIVX_Z);
    fChain->SetBranchAddress("lab2_ORIVX_XERR", &lab2_ORIVX_XERR, &b_lab2_ORIVX_XERR);
    fChain->SetBranchAddress("lab2_ORIVX_YERR", &lab2_ORIVX_YERR, &b_lab2_ORIVX_YERR);
    fChain->SetBranchAddress("lab2_ORIVX_ZERR", &lab2_ORIVX_ZERR, &b_lab2_ORIVX_ZERR);
    fChain->SetBranchAddress("lab2_ORIVX_CHI2", &lab2_ORIVX_CHI2, &b_lab2_ORIVX_CHI2);
    fChain->SetBranchAddress("lab2_ORIVX_NDOF", &lab2_ORIVX_NDOF, &b_lab2_ORIVX_NDOF);
    fChain->SetBranchAddress("lab2_ORIVX_COV_", lab2_ORIVX_COV_, &b_lab2_ORIVX_COV_);
    fChain->SetBranchAddress("lab2_IP_ORIVX", &lab2_IP_ORIVX, &b_lab2_IP_ORIVX);
    fChain->SetBranchAddress("lab2_IPCHI2_ORIVX", &lab2_IPCHI2_ORIVX, &b_lab2_IPCHI2_ORIVX);
    fChain->SetBranchAddress("lab2_P", &lab2_P, &b_lab2_P);
    fChain->SetBranchAddress("lab2_PT", &lab2_PT, &b_lab2_PT);
    fChain->SetBranchAddress("lab2_PE", &lab2_PE, &b_lab2_PE);
    fChain->SetBranchAddress("lab2_PX", &lab2_PX, &b_lab2_PX);
    fChain->SetBranchAddress("lab2_PY", &lab2_PY, &b_lab2_PY);
    fChain->SetBranchAddress("lab2_PZ", &lab2_PZ, &b_lab2_PZ);
    fChain->SetBranchAddress("lab2_M", &lab2_M, &b_lab2_M);
    fChain->SetBranchAddress("lab2_TRUEID", &lab2_TRUEID, &b_lab2_TRUEID);
    fChain->SetBranchAddress("lab2_MC_MOTHER_ID", &lab2_MC_MOTHER_ID, &b_lab2_MC_MOTHER_ID);
    fChain->SetBranchAddress("lab2_MC_MOTHER_KEY", &lab2_MC_MOTHER_KEY, &b_lab2_MC_MOTHER_KEY);
    fChain->SetBranchAddress("lab2_MC_GD_MOTHER_ID", &lab2_MC_GD_MOTHER_ID, &b_lab2_MC_GD_MOTHER_ID);
    fChain->SetBranchAddress("lab2_MC_GD_MOTHER_KEY", &lab2_MC_GD_MOTHER_KEY, &b_lab2_MC_GD_MOTHER_KEY);
    fChain->SetBranchAddress("lab2_MC_GD_GD_MOTHER_ID", &lab2_MC_GD_GD_MOTHER_ID, &b_lab2_MC_GD_GD_MOTHER_ID);
    fChain->SetBranchAddress("lab2_MC_GD_GD_MOTHER_KEY", &lab2_MC_GD_GD_MOTHER_KEY, &b_lab2_MC_GD_GD_MOTHER_KEY);
    fChain->SetBranchAddress("lab2_TRUEP_E", &lab2_TRUEP_E, &b_lab2_TRUEP_E);
    fChain->SetBranchAddress("lab2_TRUEP_X", &lab2_TRUEP_X, &b_lab2_TRUEP_X);
    fChain->SetBranchAddress("lab2_TRUEP_Y", &lab2_TRUEP_Y, &b_lab2_TRUEP_Y);
    fChain->SetBranchAddress("lab2_TRUEP_Z", &lab2_TRUEP_Z, &b_lab2_TRUEP_Z);
    fChain->SetBranchAddress("lab2_TRUEPT", &lab2_TRUEPT, &b_lab2_TRUEPT);
    fChain->SetBranchAddress("lab2_TRUEORIGINVERTEX_X", &lab2_TRUEORIGINVERTEX_X, &b_lab2_TRUEORIGINVERTEX_X);
    fChain->SetBranchAddress("lab2_TRUEORIGINVERTEX_Y", &lab2_TRUEORIGINVERTEX_Y, &b_lab2_TRUEORIGINVERTEX_Y);
    fChain->SetBranchAddress("lab2_TRUEORIGINVERTEX_Z", &lab2_TRUEORIGINVERTEX_Z, &b_lab2_TRUEORIGINVERTEX_Z);
    fChain->SetBranchAddress("lab2_TRUEENDVERTEX_X", &lab2_TRUEENDVERTEX_X, &b_lab2_TRUEENDVERTEX_X);
    fChain->SetBranchAddress("lab2_TRUEENDVERTEX_Y", &lab2_TRUEENDVERTEX_Y, &b_lab2_TRUEENDVERTEX_Y);
    fChain->SetBranchAddress("lab2_TRUEENDVERTEX_Z", &lab2_TRUEENDVERTEX_Z, &b_lab2_TRUEENDVERTEX_Z);
    fChain->SetBranchAddress("lab2_TRUEISSTABLE", &lab2_TRUEISSTABLE, &b_lab2_TRUEISSTABLE);
    fChain->SetBranchAddress("lab2_TRUETAU", &lab2_TRUETAU, &b_lab2_TRUETAU);
    fChain->SetBranchAddress("lab2_ID", &lab2_ID, &b_lab2_ID);
    fChain->SetBranchAddress("lab2_PIDe", &lab2_PIDe, &b_lab2_PIDe);
    fChain->SetBranchAddress("lab2_PIDmu", &lab2_PIDmu, &b_lab2_PIDmu);
    fChain->SetBranchAddress("lab2_PIDK", &lab2_PIDK, &b_lab2_PIDK);
    fChain->SetBranchAddress("lab2_PIDp", &lab2_PIDp, &b_lab2_PIDp);
    fChain->SetBranchAddress("lab2_ProbNNe", &lab2_ProbNNe, &b_lab2_ProbNNe);
    fChain->SetBranchAddress("lab2_ProbNNk", &lab2_ProbNNk, &b_lab2_ProbNNk);
    fChain->SetBranchAddress("lab2_ProbNNp", &lab2_ProbNNp, &b_lab2_ProbNNp);
    fChain->SetBranchAddress("lab2_ProbNNpi", &lab2_ProbNNpi, &b_lab2_ProbNNpi);
    fChain->SetBranchAddress("lab2_ProbNNmu", &lab2_ProbNNmu, &b_lab2_ProbNNmu);
    fChain->SetBranchAddress("lab2_ProbNNghost", &lab2_ProbNNghost, &b_lab2_ProbNNghost);
    fChain->SetBranchAddress("lab2_hasMuon", &lab2_hasMuon, &b_lab2_hasMuon);
    fChain->SetBranchAddress("lab2_isMuon", &lab2_isMuon, &b_lab2_isMuon);
    fChain->SetBranchAddress("lab2_hasRich", &lab2_hasRich, &b_lab2_hasRich);
    fChain->SetBranchAddress("lab2_hasCalo", &lab2_hasCalo, &b_lab2_hasCalo);
    fChain->SetBranchAddress("lab2_L0Global_Dec", &lab2_L0Global_Dec, &b_lab2_L0Global_Dec);
    fChain->SetBranchAddress("lab2_L0Global_TIS", &lab2_L0Global_TIS, &b_lab2_L0Global_TIS);
    fChain->SetBranchAddress("lab2_L0Global_TOS", &lab2_L0Global_TOS, &b_lab2_L0Global_TOS);
    fChain->SetBranchAddress("lab2_Hlt1Global_Dec", &lab2_Hlt1Global_Dec, &b_lab2_Hlt1Global_Dec);
    fChain->SetBranchAddress("lab2_Hlt1Global_TIS", &lab2_Hlt1Global_TIS, &b_lab2_Hlt1Global_TIS);
    fChain->SetBranchAddress("lab2_Hlt1Global_TOS", &lab2_Hlt1Global_TOS, &b_lab2_Hlt1Global_TOS);
    fChain->SetBranchAddress("lab2_Hlt1Phys_Dec", &lab2_Hlt1Phys_Dec, &b_lab2_Hlt1Phys_Dec);
    fChain->SetBranchAddress("lab2_Hlt1Phys_TIS", &lab2_Hlt1Phys_TIS, &b_lab2_Hlt1Phys_TIS);
    fChain->SetBranchAddress("lab2_Hlt1Phys_TOS", &lab2_Hlt1Phys_TOS, &b_lab2_Hlt1Phys_TOS);
    fChain->SetBranchAddress("lab2_Hlt2Global_Dec", &lab2_Hlt2Global_Dec, &b_lab2_Hlt2Global_Dec);
    fChain->SetBranchAddress("lab2_Hlt2Global_TIS", &lab2_Hlt2Global_TIS, &b_lab2_Hlt2Global_TIS);
    fChain->SetBranchAddress("lab2_Hlt2Global_TOS", &lab2_Hlt2Global_TOS, &b_lab2_Hlt2Global_TOS);
    fChain->SetBranchAddress("lab2_Hlt2Phys_Dec", &lab2_Hlt2Phys_Dec, &b_lab2_Hlt2Phys_Dec);
    fChain->SetBranchAddress("lab2_TRACK_Type", &lab2_TRACK_Type, &b_lab2_TRACK_Type);
    fChain->SetBranchAddress("lab2_TRACK_Key", &lab2_TRACK_Key, &b_lab2_TRACK_Key);
    fChain->SetBranchAddress("lab2_TRACK_CHI2NDOF", &lab2_TRACK_CHI2NDOF, &b_lab2_TRACK_CHI2NDOF);
    fChain->SetBranchAddress("lab2_TRACK_PCHI2", &lab2_TRACK_PCHI2, &b_lab2_TRACK_PCHI2);
    fChain->SetBranchAddress("lab2_TRACK_MatchCHI2", &lab2_TRACK_MatchCHI2, &b_lab2_TRACK_MatchCHI2);
    fChain->SetBranchAddress("lab2_TRACK_GhostProb", &lab2_TRACK_GhostProb, &b_lab2_TRACK_GhostProb);
    fChain->SetBranchAddress("lab2_TRACK_CloneDist", &lab2_TRACK_CloneDist, &b_lab2_TRACK_CloneDist);
    fChain->SetBranchAddress("lab2_TRACK_Likelihood", &lab2_TRACK_Likelihood, &b_lab2_TRACK_Likelihood);
    fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
    fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
    fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
    fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
    fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
    fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
    fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
    fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
    fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
    fChain->SetBranchAddress("HLTTCK", &HLTTCK, &b_HLTTCK);
    fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
    fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
    fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
    fChain->SetBranchAddress("PVX", PVX, &b_PVX);
    fChain->SetBranchAddress("PVY", PVY, &b_PVY);
    fChain->SetBranchAddress("PVZ", PVZ, &b_PVZ);
    fChain->SetBranchAddress("PVXERR", PVXERR, &b_PVXERR);
    fChain->SetBranchAddress("PVYERR", PVYERR, &b_PVYERR);
    fChain->SetBranchAddress("PVZERR", PVZERR, &b_PVZERR);
    fChain->SetBranchAddress("PVCHI2", PVCHI2, &b_PVCHI2);
    fChain->SetBranchAddress("PVNDOF", PVNDOF, &b_PVNDOF);
    fChain->SetBranchAddress("PVNTRACKS", PVNTRACKS, &b_PVNTRACKS);
    fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
    fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
    fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
    fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
    fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
    fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
    fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
    fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
    fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
    fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
    fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
    fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
    fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
    fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
    fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
    fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
    fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
    fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
    fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
    fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
    fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
}

Bool_t MCanalysis::Notify()
{
    // The Notify() function is called when a new file is opened. This
    // can be either for a new TTree in a TChain or when when a new TTree
    // is started when using PROOF. It is normally not necessary to make changes
    // to the generated code, but the routine can be extended by the
    // user if needed. The return value is currently not used.

    return kTRUE;
}
