#include "MCanalysis.h"

int main()
{
    TFile *f = TFile::Open("tree/mcd02kpi_27163003_refitpv.root");
    // TTree *DecayTree = static_cast<TTree*> (f->Get("DecayTreeTuple/DecayTree"));
    TTree *DecayTree = static_cast<TTree*> (f->Get("DecayTree"));
    DecayTree->Process("MCanalysis.cpp");

    return 0;
}
