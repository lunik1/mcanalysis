//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Oct  3 17:02:03 2014 by ROOT version 5.34/20
// from TTree DecayTree/DecayTree
// found on file: root://eoslhcb.cern.ch//eos/lhcb/user/l/lben/27163003/mcd02kpi_v2.root
//////////////////////////////////////////////////////////

#ifndef MCANALYSIS_H
#define MCANALYSIS_H

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.
   const Int_t kMaxlab0_ENDVERTEX_COV = 1;
   const Int_t kMaxlab0_OWNPV_COV = 1;
   const Int_t kMaxlab0_TOPPV_COV = 1;
   const Int_t kMaxlab1_OWNPV_COV = 1;
   const Int_t kMaxlab1_TOPPV_COV = 1;
   const Int_t kMaxlab1_ORIVX_COV = 1;
   const Int_t kMaxlab2_OWNPV_COV = 1;
   const Int_t kMaxlab2_TOPPV_COV = 1;
   const Int_t kMaxlab2_ORIVX_COV = 1;

class MCanalysis : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

   // Declaration of leaf types
   Double_t        lab0_MINIP;
   Double_t        lab0_MINIPCHI2;
   Double_t        lab0_MINIPNEXTBEST;
   Double_t        lab0_MINIPCHI2NEXTBEST;
   Double_t        lab0_ENDVERTEX_X;
   Double_t        lab0_ENDVERTEX_Y;
   Double_t        lab0_ENDVERTEX_Z;
   Double_t        lab0_ENDVERTEX_XERR;
   Double_t        lab0_ENDVERTEX_YERR;
   Double_t        lab0_ENDVERTEX_ZERR;
   Double_t        lab0_ENDVERTEX_CHI2;
   Int_t           lab0_ENDVERTEX_NDOF;
   Float_t         lab0_ENDVERTEX_COV_[3][3];
   Double_t        lab0_OWNPV_X;
   Double_t        lab0_OWNPV_Y;
   Double_t        lab0_OWNPV_Z;
   Double_t        lab0_OWNPV_XERR;
   Double_t        lab0_OWNPV_YERR;
   Double_t        lab0_OWNPV_ZERR;
   Double_t        lab0_OWNPV_CHI2;
   Int_t           lab0_OWNPV_NDOF;
   Float_t         lab0_OWNPV_COV_[3][3];
   Double_t        lab0_IP_OWNPV;
   Double_t        lab0_IPCHI2_OWNPV;
   Double_t        lab0_FD_OWNPV;
   Double_t        lab0_FDCHI2_OWNPV;
   Double_t        lab0_DIRA_OWNPV;
   Double_t        lab0_TOPPV_X;
   Double_t        lab0_TOPPV_Y;
   Double_t        lab0_TOPPV_Z;
   Double_t        lab0_TOPPV_XERR;
   Double_t        lab0_TOPPV_YERR;
   Double_t        lab0_TOPPV_ZERR;
   Double_t        lab0_TOPPV_CHI2;
   Int_t           lab0_TOPPV_NDOF;
   Float_t         lab0_TOPPV_COV_[3][3];
   Double_t        lab0_IP_TOPPV;
   Double_t        lab0_IPCHI2_TOPPV;
   Double_t        lab0_FD_TOPPV;
   Double_t        lab0_FDCHI2_TOPPV;
   Double_t        lab0_DIRA_TOPPV;
   Double_t        lab0_P;
   Double_t        lab0_PT;
   Double_t        lab0_PE;
   Double_t        lab0_PX;
   Double_t        lab0_PY;
   Double_t        lab0_PZ;
   Double_t        lab0_MM;
   Double_t        lab0_MMERR;
   Double_t        lab0_M;
   Int_t           lab0_BKGCAT;
   Int_t           lab0_TRUEID;
   Int_t           lab0_MC_MOTHER_ID;
   Int_t           lab0_MC_MOTHER_KEY;
   Int_t           lab0_MC_GD_MOTHER_ID;
   Int_t           lab0_MC_GD_MOTHER_KEY;
   Int_t           lab0_MC_GD_GD_MOTHER_ID;
   Int_t           lab0_MC_GD_GD_MOTHER_KEY;
   Double_t        lab0_TRUEP_E;
   Double_t        lab0_TRUEP_X;
   Double_t        lab0_TRUEP_Y;
   Double_t        lab0_TRUEP_Z;
   Double_t        lab0_TRUEPT;
   Double_t        lab0_TRUEORIGINVERTEX_X;
   Double_t        lab0_TRUEORIGINVERTEX_Y;
   Double_t        lab0_TRUEORIGINVERTEX_Z;
   Double_t        lab0_TRUEENDVERTEX_X;
   Double_t        lab0_TRUEENDVERTEX_Y;
   Double_t        lab0_TRUEENDVERTEX_Z;
   Bool_t          lab0_TRUEISSTABLE;
   Double_t        lab0_TRUETAU;
   Int_t           lab0_ID;
   Double_t        lab0_TAU;
   Double_t        lab0_TAUERR;
   Double_t        lab0_TAUCHI2;
   Bool_t          lab0_L0Global_Dec;
   Bool_t          lab0_L0Global_TIS;
   Bool_t          lab0_L0Global_TOS;
   Bool_t          lab0_Hlt1Global_Dec;
   Bool_t          lab0_Hlt1Global_TIS;
   Bool_t          lab0_Hlt1Global_TOS;
   Bool_t          lab0_Hlt1Phys_Dec;
   Bool_t          lab0_Hlt1Phys_TIS;
   Bool_t          lab0_Hlt1Phys_TOS;
   Bool_t          lab0_Hlt2Global_Dec;
   Bool_t          lab0_Hlt2Global_TIS;
   Bool_t          lab0_Hlt2Global_TOS;
   Bool_t          lab0_Hlt2Phys_Dec;
   Double_t        lab1_MINIP;
   Double_t        lab1_MINIPCHI2;
   Double_t        lab1_MINIPNEXTBEST;
   Double_t        lab1_MINIPCHI2NEXTBEST;
   Double_t        lab1_OWNPV_X;
   Double_t        lab1_OWNPV_Y;
   Double_t        lab1_OWNPV_Z;
   Double_t        lab1_OWNPV_XERR;
   Double_t        lab1_OWNPV_YERR;
   Double_t        lab1_OWNPV_ZERR;
   Double_t        lab1_OWNPV_CHI2;
   Int_t           lab1_OWNPV_NDOF;
   Float_t         lab1_OWNPV_COV_[3][3];
   Double_t        lab1_IP_OWNPV;
   Double_t        lab1_IPCHI2_OWNPV;
   Double_t        lab1_TOPPV_X;
   Double_t        lab1_TOPPV_Y;
   Double_t        lab1_TOPPV_Z;
   Double_t        lab1_TOPPV_XERR;
   Double_t        lab1_TOPPV_YERR;
   Double_t        lab1_TOPPV_ZERR;
   Double_t        lab1_TOPPV_CHI2;
   Int_t           lab1_TOPPV_NDOF;
   Float_t         lab1_TOPPV_COV_[3][3];
   Double_t        lab1_IP_TOPPV;
   Double_t        lab1_IPCHI2_TOPPV;
   Double_t        lab1_ORIVX_X;
   Double_t        lab1_ORIVX_Y;
   Double_t        lab1_ORIVX_Z;
   Double_t        lab1_ORIVX_XERR;
   Double_t        lab1_ORIVX_YERR;
   Double_t        lab1_ORIVX_ZERR;
   Double_t        lab1_ORIVX_CHI2;
   Int_t           lab1_ORIVX_NDOF;
   Float_t         lab1_ORIVX_COV_[3][3];
   Double_t        lab1_IP_ORIVX;
   Double_t        lab1_IPCHI2_ORIVX;
   Double_t        lab1_P;
   Double_t        lab1_PT;
   Double_t        lab1_PE;
   Double_t        lab1_PX;
   Double_t        lab1_PY;
   Double_t        lab1_PZ;
   Double_t        lab1_M;
   Int_t           lab1_TRUEID;
   Int_t           lab1_MC_MOTHER_ID;
   Int_t           lab1_MC_MOTHER_KEY;
   Int_t           lab1_MC_GD_MOTHER_ID;
   Int_t           lab1_MC_GD_MOTHER_KEY;
   Int_t           lab1_MC_GD_GD_MOTHER_ID;
   Int_t           lab1_MC_GD_GD_MOTHER_KEY;
   Double_t        lab1_TRUEP_E;
   Double_t        lab1_TRUEP_X;
   Double_t        lab1_TRUEP_Y;
   Double_t        lab1_TRUEP_Z;
   Double_t        lab1_TRUEPT;
   Double_t        lab1_TRUEORIGINVERTEX_X;
   Double_t        lab1_TRUEORIGINVERTEX_Y;
   Double_t        lab1_TRUEORIGINVERTEX_Z;
   Double_t        lab1_TRUEENDVERTEX_X;
   Double_t        lab1_TRUEENDVERTEX_Y;
   Double_t        lab1_TRUEENDVERTEX_Z;
   Bool_t          lab1_TRUEISSTABLE;
   Double_t        lab1_TRUETAU;
   Int_t           lab1_ID;
   Double_t        lab1_PIDe;
   Double_t        lab1_PIDmu;
   Double_t        lab1_PIDK;
   Double_t        lab1_PIDp;
   Double_t        lab1_ProbNNe;
   Double_t        lab1_ProbNNk;
   Double_t        lab1_ProbNNp;
   Double_t        lab1_ProbNNpi;
   Double_t        lab1_ProbNNmu;
   Double_t        lab1_ProbNNghost;
   Bool_t          lab1_hasMuon;
   Bool_t          lab1_isMuon;
   Bool_t          lab1_hasRich;
   Bool_t          lab1_hasCalo;
   Bool_t          lab1_L0Global_Dec;
   Bool_t          lab1_L0Global_TIS;
   Bool_t          lab1_L0Global_TOS;
   Bool_t          lab1_Hlt1Global_Dec;
   Bool_t          lab1_Hlt1Global_TIS;
   Bool_t          lab1_Hlt1Global_TOS;
   Bool_t          lab1_Hlt1Phys_Dec;
   Bool_t          lab1_Hlt1Phys_TIS;
   Bool_t          lab1_Hlt1Phys_TOS;
   Bool_t          lab1_Hlt2Global_Dec;
   Bool_t          lab1_Hlt2Global_TIS;
   Bool_t          lab1_Hlt2Global_TOS;
   Bool_t          lab1_Hlt2Phys_Dec;
   Int_t           lab1_TRACK_Type;
   Int_t           lab1_TRACK_Key;
   Double_t        lab1_TRACK_CHI2NDOF;
   Double_t        lab1_TRACK_PCHI2;
   Double_t        lab1_TRACK_MatchCHI2;
   Double_t        lab1_TRACK_GhostProb;
   Double_t        lab1_TRACK_CloneDist;
   Double_t        lab1_TRACK_Likelihood;
   Double_t        lab2_MINIP;
   Double_t        lab2_MINIPCHI2;
   Double_t        lab2_MINIPNEXTBEST;
   Double_t        lab2_MINIPCHI2NEXTBEST;
   Double_t        lab2_OWNPV_X;
   Double_t        lab2_OWNPV_Y;
   Double_t        lab2_OWNPV_Z;
   Double_t        lab2_OWNPV_XERR;
   Double_t        lab2_OWNPV_YERR;
   Double_t        lab2_OWNPV_ZERR;
   Double_t        lab2_OWNPV_CHI2;
   Int_t           lab2_OWNPV_NDOF;
   Float_t         lab2_OWNPV_COV_[3][3];
   Double_t        lab2_IP_OWNPV;
   Double_t        lab2_IPCHI2_OWNPV;
   Double_t        lab2_TOPPV_X;
   Double_t        lab2_TOPPV_Y;
   Double_t        lab2_TOPPV_Z;
   Double_t        lab2_TOPPV_XERR;
   Double_t        lab2_TOPPV_YERR;
   Double_t        lab2_TOPPV_ZERR;
   Double_t        lab2_TOPPV_CHI2;
   Int_t           lab2_TOPPV_NDOF;
   Float_t         lab2_TOPPV_COV_[3][3];
   Double_t        lab2_IP_TOPPV;
   Double_t        lab2_IPCHI2_TOPPV;
   Double_t        lab2_ORIVX_X;
   Double_t        lab2_ORIVX_Y;
   Double_t        lab2_ORIVX_Z;
   Double_t        lab2_ORIVX_XERR;
   Double_t        lab2_ORIVX_YERR;
   Double_t        lab2_ORIVX_ZERR;
   Double_t        lab2_ORIVX_CHI2;
   Int_t           lab2_ORIVX_NDOF;
   Float_t         lab2_ORIVX_COV_[3][3];
   Double_t        lab2_IP_ORIVX;
   Double_t        lab2_IPCHI2_ORIVX;
   Double_t        lab2_P;
   Double_t        lab2_PT;
   Double_t        lab2_PE;
   Double_t        lab2_PX;
   Double_t        lab2_PY;
   Double_t        lab2_PZ;
   Double_t        lab2_M;
   Int_t           lab2_TRUEID;
   Int_t           lab2_MC_MOTHER_ID;
   Int_t           lab2_MC_MOTHER_KEY;
   Int_t           lab2_MC_GD_MOTHER_ID;
   Int_t           lab2_MC_GD_MOTHER_KEY;
   Int_t           lab2_MC_GD_GD_MOTHER_ID;
   Int_t           lab2_MC_GD_GD_MOTHER_KEY;
   Double_t        lab2_TRUEP_E;
   Double_t        lab2_TRUEP_X;
   Double_t        lab2_TRUEP_Y;
   Double_t        lab2_TRUEP_Z;
   Double_t        lab2_TRUEPT;
   Double_t        lab2_TRUEORIGINVERTEX_X;
   Double_t        lab2_TRUEORIGINVERTEX_Y;
   Double_t        lab2_TRUEORIGINVERTEX_Z;
   Double_t        lab2_TRUEENDVERTEX_X;
   Double_t        lab2_TRUEENDVERTEX_Y;
   Double_t        lab2_TRUEENDVERTEX_Z;
   Bool_t          lab2_TRUEISSTABLE;
   Double_t        lab2_TRUETAU;
   Int_t           lab2_ID;
   Double_t        lab2_PIDe;
   Double_t        lab2_PIDmu;
   Double_t        lab2_PIDK;
   Double_t        lab2_PIDp;
   Double_t        lab2_ProbNNe;
   Double_t        lab2_ProbNNk;
   Double_t        lab2_ProbNNp;
   Double_t        lab2_ProbNNpi;
   Double_t        lab2_ProbNNmu;
   Double_t        lab2_ProbNNghost;
   Bool_t          lab2_hasMuon;
   Bool_t          lab2_isMuon;
   Bool_t          lab2_hasRich;
   Bool_t          lab2_hasCalo;
   Bool_t          lab2_L0Global_Dec;
   Bool_t          lab2_L0Global_TIS;
   Bool_t          lab2_L0Global_TOS;
   Bool_t          lab2_Hlt1Global_Dec;
   Bool_t          lab2_Hlt1Global_TIS;
   Bool_t          lab2_Hlt1Global_TOS;
   Bool_t          lab2_Hlt1Phys_Dec;
   Bool_t          lab2_Hlt1Phys_TIS;
   Bool_t          lab2_Hlt1Phys_TOS;
   Bool_t          lab2_Hlt2Global_Dec;
   Bool_t          lab2_Hlt2Global_TIS;
   Bool_t          lab2_Hlt2Global_TOS;
   Bool_t          lab2_Hlt2Phys_Dec;
   Int_t           lab2_TRACK_Type;
   Int_t           lab2_TRACK_Key;
   Double_t        lab2_TRACK_CHI2NDOF;
   Double_t        lab2_TRACK_PCHI2;
   Double_t        lab2_TRACK_MatchCHI2;
   Double_t        lab2_TRACK_GhostProb;
   Double_t        lab2_TRACK_CloneDist;
   Double_t        lab2_TRACK_Likelihood;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLTTCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           nPV;
   Float_t         PVX[100];   //[nPV]
   Float_t         PVY[100];   //[nPV]
   Float_t         PVZ[100];   //[nPV]
   Float_t         PVXERR[100];   //[nPV]
   Float_t         PVYERR[100];   //[nPV]
   Float_t         PVZERR[100];   //[nPV]
   Float_t         PVCHI2[100];   //[nPV]
   Float_t         PVNDOF[100];   //[nPV]
   Float_t         PVNTRACKS[100];   //[nPV]
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;

   // List of branches
   TBranch        *b_lab0_MINIP;   //!
   TBranch        *b_lab0_MINIPCHI2;   //!
   TBranch        *b_lab0_MINIPNEXTBEST;   //!
   TBranch        *b_lab0_MINIPCHI2NEXTBEST;   //!
   TBranch        *b_lab0_ENDVERTEX_X;   //!
   TBranch        *b_lab0_ENDVERTEX_Y;   //!
   TBranch        *b_lab0_ENDVERTEX_Z;   //!
   TBranch        *b_lab0_ENDVERTEX_XERR;   //!
   TBranch        *b_lab0_ENDVERTEX_YERR;   //!
   TBranch        *b_lab0_ENDVERTEX_ZERR;   //!
   TBranch        *b_lab0_ENDVERTEX_CHI2;   //!
   TBranch        *b_lab0_ENDVERTEX_NDOF;   //!
   TBranch        *b_lab0_ENDVERTEX_COV_;   //!
   TBranch        *b_lab0_OWNPV_X;   //!
   TBranch        *b_lab0_OWNPV_Y;   //!
   TBranch        *b_lab0_OWNPV_Z;   //!
   TBranch        *b_lab0_OWNPV_XERR;   //!
   TBranch        *b_lab0_OWNPV_YERR;   //!
   TBranch        *b_lab0_OWNPV_ZERR;   //!
   TBranch        *b_lab0_OWNPV_CHI2;   //!
   TBranch        *b_lab0_OWNPV_NDOF;   //!
   TBranch        *b_lab0_OWNPV_COV_;   //!
   TBranch        *b_lab0_IP_OWNPV;   //!
   TBranch        *b_lab0_IPCHI2_OWNPV;   //!
   TBranch        *b_lab0_FD_OWNPV;   //!
   TBranch        *b_lab0_FDCHI2_OWNPV;   //!
   TBranch        *b_lab0_DIRA_OWNPV;   //!
   TBranch        *b_lab0_TOPPV_X;   //!
   TBranch        *b_lab0_TOPPV_Y;   //!
   TBranch        *b_lab0_TOPPV_Z;   //!
   TBranch        *b_lab0_TOPPV_XERR;   //!
   TBranch        *b_lab0_TOPPV_YERR;   //!
   TBranch        *b_lab0_TOPPV_ZERR;   //!
   TBranch        *b_lab0_TOPPV_CHI2;   //!
   TBranch        *b_lab0_TOPPV_NDOF;   //!
   TBranch        *b_lab0_TOPPV_COV_;   //!
   TBranch        *b_lab0_IP_TOPPV;   //!
   TBranch        *b_lab0_IPCHI2_TOPPV;   //!
   TBranch        *b_lab0_FD_TOPPV;   //!
   TBranch        *b_lab0_FDCHI2_TOPPV;   //!
   TBranch        *b_lab0_DIRA_TOPPV;   //!
   TBranch        *b_lab0_P;   //!
   TBranch        *b_lab0_PT;   //!
   TBranch        *b_lab0_PE;   //!
   TBranch        *b_lab0_PX;   //!
   TBranch        *b_lab0_PY;   //!
   TBranch        *b_lab0_PZ;   //!
   TBranch        *b_lab0_MM;   //!
   TBranch        *b_lab0_MMERR;   //!
   TBranch        *b_lab0_M;   //!
   TBranch        *b_lab0_BKGCAT;   //!
   TBranch        *b_lab0_TRUEID;   //!
   TBranch        *b_lab0_MC_MOTHER_ID;   //!
   TBranch        *b_lab0_MC_MOTHER_KEY;   //!
   TBranch        *b_lab0_MC_GD_MOTHER_ID;   //!
   TBranch        *b_lab0_MC_GD_MOTHER_KEY;   //!
   TBranch        *b_lab0_MC_GD_GD_MOTHER_ID;   //!
   TBranch        *b_lab0_MC_GD_GD_MOTHER_KEY;   //!
   TBranch        *b_lab0_TRUEP_E;   //!
   TBranch        *b_lab0_TRUEP_X;   //!
   TBranch        *b_lab0_TRUEP_Y;   //!
   TBranch        *b_lab0_TRUEP_Z;   //!
   TBranch        *b_lab0_TRUEPT;   //!
   TBranch        *b_lab0_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_lab0_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_lab0_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_lab0_TRUEENDVERTEX_X;   //!
   TBranch        *b_lab0_TRUEENDVERTEX_Y;   //!
   TBranch        *b_lab0_TRUEENDVERTEX_Z;   //!
   TBranch        *b_lab0_TRUEISSTABLE;   //!
   TBranch        *b_lab0_TRUETAU;   //!
   TBranch        *b_lab0_ID;   //!
   TBranch        *b_lab0_TAU;   //!
   TBranch        *b_lab0_TAUERR;   //!
   TBranch        *b_lab0_TAUCHI2;   //!
   TBranch        *b_lab0_L0Global_Dec;   //!
   TBranch        *b_lab0_L0Global_TIS;   //!
   TBranch        *b_lab0_L0Global_TOS;   //!
   TBranch        *b_lab0_Hlt1Global_Dec;   //!
   TBranch        *b_lab0_Hlt1Global_TIS;   //!
   TBranch        *b_lab0_Hlt1Global_TOS;   //!
   TBranch        *b_lab0_Hlt1Phys_Dec;   //!
   TBranch        *b_lab0_Hlt1Phys_TIS;   //!
   TBranch        *b_lab0_Hlt1Phys_TOS;   //!
   TBranch        *b_lab0_Hlt2Global_Dec;   //!
   TBranch        *b_lab0_Hlt2Global_TIS;   //!
   TBranch        *b_lab0_Hlt2Global_TOS;   //!
   TBranch        *b_lab0_Hlt2Phys_Dec;   //!
   TBranch        *b_lab1_MINIP;   //!
   TBranch        *b_lab1_MINIPCHI2;   //!
   TBranch        *b_lab1_MINIPNEXTBEST;   //!
   TBranch        *b_lab1_MINIPCHI2NEXTBEST;   //!
   TBranch        *b_lab1_OWNPV_X;   //!
   TBranch        *b_lab1_OWNPV_Y;   //!
   TBranch        *b_lab1_OWNPV_Z;   //!
   TBranch        *b_lab1_OWNPV_XERR;   //!
   TBranch        *b_lab1_OWNPV_YERR;   //!
   TBranch        *b_lab1_OWNPV_ZERR;   //!
   TBranch        *b_lab1_OWNPV_CHI2;   //!
   TBranch        *b_lab1_OWNPV_NDOF;   //!
   TBranch        *b_lab1_OWNPV_COV_;   //!
   TBranch        *b_lab1_IP_OWNPV;   //!
   TBranch        *b_lab1_IPCHI2_OWNPV;   //!
   TBranch        *b_lab1_TOPPV_X;   //!
   TBranch        *b_lab1_TOPPV_Y;   //!
   TBranch        *b_lab1_TOPPV_Z;   //!
   TBranch        *b_lab1_TOPPV_XERR;   //!
   TBranch        *b_lab1_TOPPV_YERR;   //!
   TBranch        *b_lab1_TOPPV_ZERR;   //!
   TBranch        *b_lab1_TOPPV_CHI2;   //!
   TBranch        *b_lab1_TOPPV_NDOF;   //!
   TBranch        *b_lab1_TOPPV_COV_;   //!
   TBranch        *b_lab1_IP_TOPPV;   //!
   TBranch        *b_lab1_IPCHI2_TOPPV;   //!
   TBranch        *b_lab1_ORIVX_X;   //!
   TBranch        *b_lab1_ORIVX_Y;   //!
   TBranch        *b_lab1_ORIVX_Z;   //!
   TBranch        *b_lab1_ORIVX_XERR;   //!
   TBranch        *b_lab1_ORIVX_YERR;   //!
   TBranch        *b_lab1_ORIVX_ZERR;   //!
   TBranch        *b_lab1_ORIVX_CHI2;   //!
   TBranch        *b_lab1_ORIVX_NDOF;   //!
   TBranch        *b_lab1_ORIVX_COV_;   //!
   TBranch        *b_lab1_IP_ORIVX;   //!
   TBranch        *b_lab1_IPCHI2_ORIVX;   //!
   TBranch        *b_lab1_P;   //!
   TBranch        *b_lab1_PT;   //!
   TBranch        *b_lab1_PE;   //!
   TBranch        *b_lab1_PX;   //!
   TBranch        *b_lab1_PY;   //!
   TBranch        *b_lab1_PZ;   //!
   TBranch        *b_lab1_M;   //!
   TBranch        *b_lab1_TRUEID;   //!
   TBranch        *b_lab1_MC_MOTHER_ID;   //!
   TBranch        *b_lab1_MC_MOTHER_KEY;   //!
   TBranch        *b_lab1_MC_GD_MOTHER_ID;   //!
   TBranch        *b_lab1_MC_GD_MOTHER_KEY;   //!
   TBranch        *b_lab1_MC_GD_GD_MOTHER_ID;   //!
   TBranch        *b_lab1_MC_GD_GD_MOTHER_KEY;   //!
   TBranch        *b_lab1_TRUEP_E;   //!
   TBranch        *b_lab1_TRUEP_X;   //!
   TBranch        *b_lab1_TRUEP_Y;   //!
   TBranch        *b_lab1_TRUEP_Z;   //!
   TBranch        *b_lab1_TRUEPT;   //!
   TBranch        *b_lab1_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_lab1_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_lab1_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_lab1_TRUEENDVERTEX_X;   //!
   TBranch        *b_lab1_TRUEENDVERTEX_Y;   //!
   TBranch        *b_lab1_TRUEENDVERTEX_Z;   //!
   TBranch        *b_lab1_TRUEISSTABLE;   //!
   TBranch        *b_lab1_TRUETAU;   //!
   TBranch        *b_lab1_ID;   //!
   TBranch        *b_lab1_PIDe;   //!
   TBranch        *b_lab1_PIDmu;   //!
   TBranch        *b_lab1_PIDK;   //!
   TBranch        *b_lab1_PIDp;   //!
   TBranch        *b_lab1_ProbNNe;   //!
   TBranch        *b_lab1_ProbNNk;   //!
   TBranch        *b_lab1_ProbNNp;   //!
   TBranch        *b_lab1_ProbNNpi;   //!
   TBranch        *b_lab1_ProbNNmu;   //!
   TBranch        *b_lab1_ProbNNghost;   //!
   TBranch        *b_lab1_hasMuon;   //!
   TBranch        *b_lab1_isMuon;   //!
   TBranch        *b_lab1_hasRich;   //!
   TBranch        *b_lab1_hasCalo;   //!
   TBranch        *b_lab1_L0Global_Dec;   //!
   TBranch        *b_lab1_L0Global_TIS;   //!
   TBranch        *b_lab1_L0Global_TOS;   //!
   TBranch        *b_lab1_Hlt1Global_Dec;   //!
   TBranch        *b_lab1_Hlt1Global_TIS;   //!
   TBranch        *b_lab1_Hlt1Global_TOS;   //!
   TBranch        *b_lab1_Hlt1Phys_Dec;   //!
   TBranch        *b_lab1_Hlt1Phys_TIS;   //!
   TBranch        *b_lab1_Hlt1Phys_TOS;   //!
   TBranch        *b_lab1_Hlt2Global_Dec;   //!
   TBranch        *b_lab1_Hlt2Global_TIS;   //!
   TBranch        *b_lab1_Hlt2Global_TOS;   //!
   TBranch        *b_lab1_Hlt2Phys_Dec;   //!
   TBranch        *b_lab1_TRACK_Type;   //!
   TBranch        *b_lab1_TRACK_Key;   //!
   TBranch        *b_lab1_TRACK_CHI2NDOF;   //!
   TBranch        *b_lab1_TRACK_PCHI2;   //!
   TBranch        *b_lab1_TRACK_MatchCHI2;   //!
   TBranch        *b_lab1_TRACK_GhostProb;   //!
   TBranch        *b_lab1_TRACK_CloneDist;   //!
   TBranch        *b_lab1_TRACK_Likelihood;   //!
   TBranch        *b_lab2_MINIP;   //!
   TBranch        *b_lab2_MINIPCHI2;   //!
   TBranch        *b_lab2_MINIPNEXTBEST;   //!
   TBranch        *b_lab2_MINIPCHI2NEXTBEST;   //!
   TBranch        *b_lab2_OWNPV_X;   //!
   TBranch        *b_lab2_OWNPV_Y;   //!
   TBranch        *b_lab2_OWNPV_Z;   //!
   TBranch        *b_lab2_OWNPV_XERR;   //!
   TBranch        *b_lab2_OWNPV_YERR;   //!
   TBranch        *b_lab2_OWNPV_ZERR;   //!
   TBranch        *b_lab2_OWNPV_CHI2;   //!
   TBranch        *b_lab2_OWNPV_NDOF;   //!
   TBranch        *b_lab2_OWNPV_COV_;   //!
   TBranch        *b_lab2_IP_OWNPV;   //!
   TBranch        *b_lab2_IPCHI2_OWNPV;   //!
   TBranch        *b_lab2_TOPPV_X;   //!
   TBranch        *b_lab2_TOPPV_Y;   //!
   TBranch        *b_lab2_TOPPV_Z;   //!
   TBranch        *b_lab2_TOPPV_XERR;   //!
   TBranch        *b_lab2_TOPPV_YERR;   //!
   TBranch        *b_lab2_TOPPV_ZERR;   //!
   TBranch        *b_lab2_TOPPV_CHI2;   //!
   TBranch        *b_lab2_TOPPV_NDOF;   //!
   TBranch        *b_lab2_TOPPV_COV_;   //!
   TBranch        *b_lab2_IP_TOPPV;   //!
   TBranch        *b_lab2_IPCHI2_TOPPV;   //!
   TBranch        *b_lab2_ORIVX_X;   //!
   TBranch        *b_lab2_ORIVX_Y;   //!
   TBranch        *b_lab2_ORIVX_Z;   //!
   TBranch        *b_lab2_ORIVX_XERR;   //!
   TBranch        *b_lab2_ORIVX_YERR;   //!
   TBranch        *b_lab2_ORIVX_ZERR;   //!
   TBranch        *b_lab2_ORIVX_CHI2;   //!
   TBranch        *b_lab2_ORIVX_NDOF;   //!
   TBranch        *b_lab2_ORIVX_COV_;   //!
   TBranch        *b_lab2_IP_ORIVX;   //!
   TBranch        *b_lab2_IPCHI2_ORIVX;   //!
   TBranch        *b_lab2_P;   //!
   TBranch        *b_lab2_PT;   //!
   TBranch        *b_lab2_PE;   //!
   TBranch        *b_lab2_PX;   //!
   TBranch        *b_lab2_PY;   //!
   TBranch        *b_lab2_PZ;   //!
   TBranch        *b_lab2_M;   //!
   TBranch        *b_lab2_TRUEID;   //!
   TBranch        *b_lab2_MC_MOTHER_ID;   //!
   TBranch        *b_lab2_MC_MOTHER_KEY;   //!
   TBranch        *b_lab2_MC_GD_MOTHER_ID;   //!
   TBranch        *b_lab2_MC_GD_MOTHER_KEY;   //!
   TBranch        *b_lab2_MC_GD_GD_MOTHER_ID;   //!
   TBranch        *b_lab2_MC_GD_GD_MOTHER_KEY;   //!
   TBranch        *b_lab2_TRUEP_E;   //!
   TBranch        *b_lab2_TRUEP_X;   //!
   TBranch        *b_lab2_TRUEP_Y;   //!
   TBranch        *b_lab2_TRUEP_Z;   //!
   TBranch        *b_lab2_TRUEPT;   //!
   TBranch        *b_lab2_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_lab2_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_lab2_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_lab2_TRUEENDVERTEX_X;   //!
   TBranch        *b_lab2_TRUEENDVERTEX_Y;   //!
   TBranch        *b_lab2_TRUEENDVERTEX_Z;   //!
   TBranch        *b_lab2_TRUEISSTABLE;   //!
   TBranch        *b_lab2_TRUETAU;   //!
   TBranch        *b_lab2_ID;   //!
   TBranch        *b_lab2_PIDe;   //!
   TBranch        *b_lab2_PIDmu;   //!
   TBranch        *b_lab2_PIDK;   //!
   TBranch        *b_lab2_PIDp;   //!
   TBranch        *b_lab2_ProbNNe;   //!
   TBranch        *b_lab2_ProbNNk;   //!
   TBranch        *b_lab2_ProbNNp;   //!
   TBranch        *b_lab2_ProbNNpi;   //!
   TBranch        *b_lab2_ProbNNmu;   //!
   TBranch        *b_lab2_ProbNNghost;   //!
   TBranch        *b_lab2_hasMuon;   //!
   TBranch        *b_lab2_isMuon;   //!
   TBranch        *b_lab2_hasRich;   //!
   TBranch        *b_lab2_hasCalo;   //!
   TBranch        *b_lab2_L0Global_Dec;   //!
   TBranch        *b_lab2_L0Global_TIS;   //!
   TBranch        *b_lab2_L0Global_TOS;   //!
   TBranch        *b_lab2_Hlt1Global_Dec;   //!
   TBranch        *b_lab2_Hlt1Global_TIS;   //!
   TBranch        *b_lab2_Hlt1Global_TOS;   //!
   TBranch        *b_lab2_Hlt1Phys_Dec;   //!
   TBranch        *b_lab2_Hlt1Phys_TIS;   //!
   TBranch        *b_lab2_Hlt1Phys_TOS;   //!
   TBranch        *b_lab2_Hlt2Global_Dec;   //!
   TBranch        *b_lab2_Hlt2Global_TIS;   //!
   TBranch        *b_lab2_Hlt2Global_TOS;   //!
   TBranch        *b_lab2_Hlt2Phys_Dec;   //!
   TBranch        *b_lab2_TRACK_Type;   //!
   TBranch        *b_lab2_TRACK_Key;   //!
   TBranch        *b_lab2_TRACK_CHI2NDOF;   //!
   TBranch        *b_lab2_TRACK_PCHI2;   //!
   TBranch        *b_lab2_TRACK_MatchCHI2;   //!
   TBranch        *b_lab2_TRACK_GhostProb;   //!
   TBranch        *b_lab2_TRACK_CloneDist;   //!
   TBranch        *b_lab2_TRACK_Likelihood;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLTTCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_nPV;   //!
   TBranch        *b_PVX;   //!
   TBranch        *b_PVY;   //!
   TBranch        *b_PVZ;   //!
   TBranch        *b_PVXERR;   //!
   TBranch        *b_PVYERR;   //!
   TBranch        *b_PVZERR;   //!
   TBranch        *b_PVCHI2;   //!
   TBranch        *b_PVNDOF;   //!
   TBranch        *b_PVNTRACKS;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!

   MCanalysis(TTree * /*tree*/ =0) : fChain(0) { }
   virtual ~MCanalysis() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(MCanalysis,0);

protected:
   std::string GetDate();
   virtual void TauHist();
   virtual Int_t EqualLifetimeBins();
   virtual void LifetimeBinnedMassHists(const Int_t binSize);
   virtual void LifetimeBinnedIPChi2Hists(const Int_t binSize);
   virtual void LifetimeBinnedThetaHists(const Int_t binSize);
   virtual void LifetimeBinnedPrimaryHists(const Int_t binSize);
   virtual void LifetimeBinnedSecondaryHists(const Int_t binSize);
   virtual void MassHist();
   virtual void MassHistCut();
   virtual void MassFit();
   virtual void MassFitCut();
   virtual void IPChi2Hist();
   virtual void IPChi2HistCut();
   virtual void IPChi2Fit();
   virtual void IPChi2FitCut();
   virtual void MassIPChi2Hist();
};

#endif
